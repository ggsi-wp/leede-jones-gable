<?php
/**
 * For building functions
 */


$start      = date( 'Y' );
$years      = range( $start, $start-20, -1 );
$months     = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

function renderAccountOptions( $clientId )
{   
   
    $dAccounts  = unserialize( get_post_meta( $clientId, 'client_doxim_accts', TRUE ) ); 
    if( count( $dAccounts ) > 1 ): ?>
        <option value="-1">All Accounts</option>
    <?php endif;
    foreach( $dAccounts as $d ): ?>
        <option value="<?php echo $d; ?>"><?php echo $d; ?></option>
    <?php endforeach;
}

function renderMonthOptions()
{
    $months     = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]; ?>
    <option value="-1">All</option> 
    <?php
        foreach( $months as $k => $m ): ?>
            <option value="<?php echo $k; ?>"><?php echo $m; ?></option>
    <?php endforeach;
}

function renderYearOptions( $yRange = 20 )
{
    $start      = date( 'Y' );
    $years      = range( $start, $start-20, -1 ); 
    foreach( $years as $y ): ?>
        <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
    <?php endforeach;   
}