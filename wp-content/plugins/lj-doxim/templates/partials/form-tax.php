<?php

function form_tax( $clientId )
{   
    ob_start();
    ?>


    <form method="post" action="<?php echo $base_url . '?tab=tax'; ?>">
        <div>
            <label for="account-no">Accounts</label>
            <select name="account-no" id="account-no">
                <?php renderAccountOptions( $clientId ); ?>
            </select>
        </div>
        <div><input type="radio" name="range" value="0" checked="checked">Show 10 most recent</div>
        <div><input type="radio" name="range" value="1">Tax Year
            <select name="year">
                <?php renderYearOptions( $yRange ); ?>
            </select>
        </div>
        <div><input type="radio" name="range" value="2">or Year Published
            <select name="year-pub">
                <?php renderYearOptions( $yRange ); ?>
            </select>            
        </div>
        <div>
            <input type="submit" name="submit" value="Go" />
        </div>
    </form>
    <?php
    $o  = ob_get_clean();

    echo $o;
}        