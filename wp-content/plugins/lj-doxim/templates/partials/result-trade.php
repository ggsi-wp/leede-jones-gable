<?php
    if( isset( $results ) ):
        if( count( $results ) > 0 ):         
        ?>

        <table>
            <thead>
                <tr>
                    <td>Document Name</td>
                    <td>Company</td>
                    <td>Account #</td>
                    <td>Date</td>
                    <td>Options</td>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach( $results as $row ): 
                        $str    = $row['docUrl'] . $row->attributes()->id;//$row['docUrl'] . '&docid=' . $row->attributes()->id;
                        $estr   = my_simple_crypt( $str, 'e' );                    
                    ?>
                        <tr>
                            <td><?php echo $row->column[5]; ?></td>
                            <td></td>
                            <td><?php echo $row->column[4]; ?></td>
                            <td><?php echo $row->column[3]; ?></td>
                            <td><a href="<?php echo site_url(); ?>/doxim-document-download-page?docid=<?php echo $estr; ?>" target="_blank">Download</a></td>
                <?php endforeach; ?>       
            </tbody>
        </table>
    <?php else: ?>
        <p>No results</p>    
    <?php endif;
    endif; ?>  