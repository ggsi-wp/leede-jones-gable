<style>
    a.active { font-weight: bold; }
</style>
<h2>My Documents</h2>
<?php 

    $tab        = isset( $_GET['tab'] ) ? $_GET['tab'] : 'account'; 

    $userId     = get_current_user_id();

    $args   = [
        'post_type'     => 'client',
        'meta_key'      => 'client_user_id',
        'meta_value'    => $userId
    ];
    $query  = new WP_Query( $args );
    $clientId   = $query->posts[0]->ID;
    

    $yRange     = 20; // [FIX] - Add to settings

    

    /**
     * Get content if requested
     */
    if( isset( $_POST['submit'] ) )
    {
        
        // What account or accounts?
        $accounts   = sanitize_text_field( $_POST['account-no'] );
        if( $accounts < 0 ) // All Accounts
        {
            $accounts   = unserialize( get_post_meta( $clientId, 'client_doxim_accts', TRUE ) );
        }
        else
        {
            $accounts   = (array) $accounts;
        }
        
        // Fetch data
        $results    = fetchXmlData( $accounts, $tab );
 
    }

    

    include_once( 'form-building.php' ); 
    include_once( 'partials/navigation.php' );
    include_once( "partials/form-{$tab}.php" );
    call_user_func( 'form_' . $tab, $clientId );    
    
    include_once( "partials/result-{$tab}.php" ); 
    

    
    
    /**
     * Create date range string if requested
     */
    function createDateStr( $tab )
    {
        
        $o  = '';
        switch( $tab )
        {
            case 'account': 
            case 'reports':
                    $year   = sanitize_text_field( $_POST['year'] );
                    $month  = sanitize_text_field( $_POST['month'] );
                    if( -1 == $month )
                    {
                        $o  = '01/01/' . $year . '-31/12/' . $year;
                    }
                    else
                    {
                        $o  =  '01/' . str_pad( $month, 2, '0', STR_PAD_LEFT ) . '/' . $year . '-' . cal_days_in_month( CAL_GREGORIAN, $month, $year ) . '/' . $month . '/' . $year;
                    }
                break;
            case 'trade':
                    if( 1 == $_POST['range'] )
                    {
                        $days   = sanitize_text_field( $_POST['days'] );
                        $o  = date( 'd/m/Y' ) .  '-' . date( 'd/m/Y', strtotime( "-" . $days . "day" ) );
                    }
                    else    // Assumes 2 
                    {
                        $year   = sanitize_text_field( $_POST['year'] );
                        $month  = sanitize_text_field( $_POST['month'] );
                        if( -1 == $month )
                        {
                            $o  = '01/01/' . $year . '-31/12/' . $year;
                        }
                        else
                        {
                            $o  =  '01/' . str_pad( $month, 2, '0', STR_PAD_LEFT ) . '/' . $year . '-' . cal_days_in_month( CAL_GREGORIAN, $month, $year ) . '/' . $month . '/' . $year;
                        }                        
                    }
                break;
            case 'tax':
                    if( 1 == $_POST['range'] )
                    {
                        $year   = sanitize_text_field( $_POST['year'] );
                    }
                    else
                    {
                        $year   = sanitize_text_field( $_POST['year-pub'] );
                    }
                    $o  = '01/01/' . $year . '-31/12/' . $year;
                break;
        }

        return $o;
    }

    /**
     *  Fetch XML data from provided source
     */
    function fetchXmlData( $accounts, $tab )
    {   
        
        $bin    = esc_attr( get_option('doxim_bin') );
        $passwd = esc_attr( get_option('doxim_password') );
        $apiUrl = esc_attr( get_option('doxim_apiurl') ); 
        $viewId = esc_attr( get_option('doxim_' . $tab . '_view_id') );
        $maxCount   = esc_attr( get_option( $tab . 'Count') );

        // Create a stream

       $dargs=array(
           "ssl"=>array(
               "verify_peer"=>false,
               "verify_peer_name"=>false),
            "http"=>array(
                'timeout' => 60, 
                'user_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/3.0.0.1'
            )
        );
  
       $context    = stream_context_create( $dargs );


       if( !empty( $_POST['range'] ) )    // Range value not 0
       {
           $dateStr     = createDateStr( $tab );
           $dateStr     .= '&dates=' . $dateStr;
           $maxCount    = 99;
       }
       else
       {
           $dateStr     = '';
       } 

       $o   = []; 
       foreach( $accounts as $acct )
       {
            if( empty( $acct ) ) continue;
            $apiGetDocs = $apiUrl . "bin=" . $bin . '&passwd=' . $passwd . "&viewid=" . $viewId . "&mid=" . $acct . $dateStr ; //var_dump( $apiGetDocs );
            echo '<!-- xmlurl: ' . $apiGetDocs . ' -->';

            if (($response_xml_data = file_get_contents( $apiGetDocs, false, $context ))===false)
            {
                echo "Error fetching XML\n";
            } 
            else 
            {
               libxml_use_internal_errors(true);
               $data = simplexml_load_string($response_xml_data);
               if (!$data) 
               {
                   echo "Error loading XML\n";
                   foreach(libxml_get_errors() as $error) 
                   {
                       echo "\t", $error->message;
                   }
               } 
               else 
               { 
                    $docUrl = $viewId . '_' . $acct . '_'; //"viewid=" . $viewId . "&mid=" . $acct;
                    $o      = array_merge( $o, organiseData( $data, $docUrl ) );
               }
            }            
            
       }
       usort( $o, function( $a, $b ){
            return strtotime( $a->column[3] ) - strtotime( $b->column[3] );
       } ); 
       $o  = array_slice( $o, 0, $maxCount );
       return $o;
    }


    /**
     * Organise the XML data 
     */
    function organiseData( $data, $docUrl )
    {   
        $o  = [];
        foreach( $data->rows->row as $a )
        {
            $a['docUrl']    = $docUrl;
            $o[] = $a;
        } 
        return $o;
    }

