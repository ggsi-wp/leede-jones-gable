<?php
/**
 * For building functions
 */


$start      = date( 'Y' );
$years      = range( $start, $start-20, -1 );
$months     = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ];

function renderClientsOptions( $dAccounts )
{   
    if( count( $dAccounts ) > 1 ): ?>
        <option value="-1">All Clients</option>
    <?php endif;
    foreach( $dAccounts as $d ): ?>
        
		<option <?php if(strcasecmp($_POST['account-no'], $d) == 0) { echo "selected=selected"; } ?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
    <?php endforeach;
}

function renderAccountOptions( $dAccounts )
{   
    if( count( $dAccounts ) > 1 ): ?>
        <option value="-1">All Accounts</option>
    <?php endif;
    foreach( $dAccounts as $d ): ?>
        
		<option <?php if(strcasecmp($_POST['account-no'], $d) == 0) { echo "selected=selected"; } ?> value="<?php echo $d; ?>"><?php echo $d; ?></option>
    <?php endforeach;
}

function renderMonthOptions()
{
    $months     = ["All",'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]; ?>
    
    <?php
        foreach( $months as $k => $m ): ?>
            
			<option <?php if($_POST['ljmonth'] == $k) { echo "selected=selected"; } ?> value="<?php echo $k; ?>"><?php echo $m; ?></option>
    <?php endforeach;
}

function renderYearOptions( $yRange = 20 )
{
    $start      = date( 'Y' );
    $years      = range( $start, $start-20, -1 ); 
    foreach( $years as $y ): ?>
        
		<option <?php if($_POST['ljyear'] == $y) { echo "selected=selected"; } ?> value="<?php echo $y; ?>"><?php echo $y; ?></option>

    <?php endforeach;   
}