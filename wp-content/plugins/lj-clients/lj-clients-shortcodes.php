<?php
/**
 * Register Client page Shortcodes
 */


add_shortcode( 'lj-client-dashboard', 'client_dashboard_creation' );
add_shortcode( 'lj-client-manage-account', 'client_manage_account' );
add_shortcode( 'lj-client-restriction', 'client_page_restriction' );
add_shortcode( 'lj-client-change-password' , 'client_change_password' );
add_shortcode( 'lj-client-navigation', 'client_navigation' );
add_shortcode( 'lj-client-login', 'client_login' );
add_shortcode( 'lj-client-advisor', 'client_advisor' );
add_shortcode( 'lj-doxim-page', 'doxim_page' );
add_shortcode( 'lj-client-welcome', 'lj_welcome_message' );
 

 /**
 * Shortcode content
 */
function client_dashboard_creation()
{
    include_once( 'templates/dashboard.php' );
    return dashboard();
}

function client_manage_account()
{
    include_once( 'templates/manage-account.php' );
    echo displayAccount();
}

function client_page_restriction()
{
    
    /*
    * [JSA:14-06-19] Short code suppressed in favour of a bit of code in lj-clients.php that redirects earlier in the process

    $userId = get_current_user_id(); 
    $userdata   = get_userdata( $userId );
    if( !WP_DEBUG && ( empty( $userId ) || !in_array( 'client', $userdata->roles ) ) )
    {
        if( headers_sent() )
        {
            ob_start(); ?>
                <script>location.href="<?php echo site_url(); ?>/login"</script>
            <?php return ob_get_clean();
        }
        else{
            $o  = `
                header( "Location: " . site_url() . "/login" );
                die();
                `;
            return $o;
        }
    }
    $clientId   = getClientId( $userId ); //var_dump( !WP_DEBUG && !empty( esc_attr( get_post_meta( $clientId, 'client_password_change', true ) ) ) && FALSE == strpos( $_SERVER["REQUEST_URI"], 'change-password' ) ); exit( 'clientId: ' . $clientId );
    if( !WP_DEBUG && !empty( esc_attr( get_post_meta( $clientId, 'client_password_change', true ) ) ) && FALSE == strpos( $_SERVER["REQUEST_URI"], 'change-password' ) )
    {
        if( headers_sent() )
        {
            ob_start(); ?>
                <script>location.href="<?php echo site_url(); ?>/change-password"</script>
            <?php return ob_get_clean(); 
        }
        else{
            $o  = `
                header( "Location: " . site_url() . '/change-password' );
                die();
            `;
            return $o;
        }        
    }*/
}

function client_change_password()
{
    include_once( 'templates/change-password.php' );
    return \LJClient\Security\change_password();
}

function client_navigation()
{
    include_once( 'templates/client-navigation.php' );
    return \LJClient\Navigation\displayNavigation();
}

function client_login( $atts=[] )
{

    // normalize attribute keys, lowercase
    $atts = array_change_key_case( (array)$atts, CASE_LOWER );
 
    // override default attributes with user attributes
    $atts = shortcode_atts([
                    'lang' => 'en',
    ], $atts, $tag);    
    
    include_once( 'templates/partials/client-login.php' );
    return \LJClient\Navigation\displayLogin( $atts['lang'] );
}

/**
 * Advisor contact page
 */
function client_advisor()
{   
    include_once( 'templates/partials/advisor.php' );
    return displayAdvisor();
}

/**
 * Doxim account pages
 */
function doxim_page( $atts=[] )
{   
    // override default attributes with user attributes
    $atts = shortcode_atts([
                    'viewid'    => 73,
                    'mincount'  => 20,
                    'type'      => 'account'    // account, trade, tax, reports, sma
    ], $atts, $tag);   
    
    $res    = include_once( 'templates/doxim-page.php' ); 
    return \LJClient\Doxim\displayDoximPage( $atts );    
}

function lj_welcome_message(){
    include_once( 'templates/welcome.php' );
    return displayWelcome(); 
}
