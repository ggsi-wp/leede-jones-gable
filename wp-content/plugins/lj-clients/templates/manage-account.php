<?php
/**
 * Template for Client Dashboard
 */

function displayAccount()
{   
    $userId = get_current_user_id();
    // Need to get clientId where 'client_user_id' == $userId;
    $clientId   = getClientId( $userId );
    $action     = 'manage-account';
    $changed    = FALSE;

    if( isset( $_POST['change-password'] ) )
    {
        if( empty( $_POST['new-password'] ) || empty( $_POST['new-password-conf'] ) )
        {   
            $passErrors[]    = __( 'Both Password and Password Confirmation are required' );
        }
        else
        {
            $password   = sanitize_text_field( $_POST['new-password'] );
            $passconf   = sanitize_text_field( $_POST['new-password-conf'] );
            if( $password !== $passconf )
            {
                $passErrors[] = __( 'Password and Password Confirmation must match' );
            }
            else
            {
                wp_set_password( $password, $userId );
                update_post_meta( $clientId, 'client_password_change', 0 );
                $passMessages[] = 'Password has been changed. <a href="' . site_url() . '/login">Go to Login?</a>';
                $changed        = TRUE;
            }
        } 
    }

    ob_start();
        if( $changed ):
    ?>
        <p>Your password has been successfully changed. <a href="<?php echo site_url(); ?>/login">Go to Login?</a></p>
        <?php else: ?>

        <h2>Manage your account</h2>
        <?php include_once( 'partials/change-name.php' ); ?>
        <?php include_once( 'partials/change-password.php' ); ?>
        <?php //include_once( 'partials/change-questions.php' ); ?>

    <?php 
        endif; 
    return ob_get_clean(); 
}