<?php
/**
 * Navigation for Client Sidebar
 * It's been changed so many times it's easier to pull it out than to try to maintain it from the WP Dashboard
 */
namespace LJClient\Navigation; 

function displayNavigation()
{   
/*    $userId     = get_current_user_id(); 
    $clientId   = getClientId( $userId ); */
    $doxim      = get_post_meta( $_SESSION['clientId'], 'client_doxim', TRUE ); 
    
    ob_start(); ?>
        <div class="welcome">
            <div class="welcome-wrapper">
                <?php if( !isset( $_SESSION['pwchange'] ) ): // If password change is not required ?>
                    <?php if( !empty( $doxim ) ):?>
                        <article>
                            <div class="welcome_roundel"><img src="<?php echo site_url(); ?>/wp-content/plugins/lj-clients/templates/assets/documents.png" /></div>
                            <a href="<?php echo site_url(); ?>/account-statements">
                            <h3>Documents</h3>
                            </a>
                            <?php wp_nav_menu( ['menu' => 'Client Centre']  ); ?>
                        </article>
                    <?php endif; ?>
                    <?php if( $_SESSION['broadridge'] ): ?>
                        <article>
                            <div class="welcome_roundel"><img src="<?php echo site_url(); ?>/wp-content/plugins/lj-clients/templates/assets/accounts.png" /></div>
                            <a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/holdings/holdaccts.htm">
                            <h3>Account Status</h3>
                            </a>
                            <ul>
                                <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/holdings/holdaccts.htm">Holdings</a></li>
                                <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/portfolios/portaccts.htm">Portfolio View</a></li>
                                <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/activity/actaccts.htm">Activity</a></li>
                                <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/orderbook/obaccts.htm">Pending Trades</a></li>
                                <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/activity/history.htm">History</a></li>
                                <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/plan/planaccts.htm">Plan Data</a></li>
                                <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/other/nicknames.htm">Nicknames</a></li>
                            </ul>
                        </article>   
                    <?php endif; ?> 
                <?php endif; ?>    
            </div>
        </div>
    <?php
        return ob_get_clean();         

}