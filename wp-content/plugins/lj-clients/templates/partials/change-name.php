<?php
/**
 * Name change statement
 */
$firstname  = get_post_meta( $clientId, 'client_firstname', TRUE );
$lastname   = get_post_meta( $clientId, 'client_lastname', TRUE );
$name       = $firstname .  ' ' . $lastname;
$email      = get_post_meta( $clientId, 'client_email', TRUE );

?>
<div class="welcome">
    To make changes to your name or email, <a href="<?php echo site_url(); ?>/client-advisor">please contact your investment advisor</a></p>
</div>
<div class="welcome">
    <p>Your Email: <?php echo $email; ?></p>
    <p>Your Name: <?php echo $name; ?></p>
</div>