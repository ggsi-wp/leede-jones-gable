<?php
/**
 * Top login buttons
 */


namespace LJClient\Navigation;

function displayLogin( $lang )
{   
    ob_start(); 
        switch( $lang )
        {
            
            case 'en': 
    ?>
                <div class="header-widget widget_nav_menu header-right-widget">
                    <div class="lj-login-wrapper">
                        <div class="switch-lang"><a href="<?php echo site_url(); ?>/fr/">Francais</a></div>
                        <div class="lj-login">
                            <?php if( is_user_logged_in() ):
                            //    $userId     = get_current_user_id();
                            //    $clientId   = getClientId( $userId );
                            //    $broadridge = get_post_meta( $clientId, 'client_broadridge', TRUE ); ?>
                                <?php if( !isset( $_SESSION['pwchange'] ) ): ?>
                                    <a href="<?php echo site_url(); ?>/client-dashboard" class="lj-btn">Dashboard</a>
                                <?php endif; ?>
                                <?php if( $_SESSION['broadridge'] ): ?>    
                                        <a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=other/logout.htm" class="lj-btn">
                                    <?php else: ?>
                                        <a href="<?php echo wp_logout_url(); ?>" class="lj-btn">
                                    <?php endif; ?>
                                    Logout</a>

                            <?php else: ?>
                                <a href="<?php echo site_url(); ?>/login" class="lj-btn">Client Login</a>
                            <?php endif; ?>
                        </div>
                    <div>
                </div>    
    <?php  
                break;
            case 'fr':
    ?>
                <div class="header-widget widget_nav_menu header-right-widget">
                    <div class="lj-login-wrapper">
                        <div class="switch-lang"><a href="">Francais</a></div>
                        <div class="lj-login">
                            <a href="" class="lj-btn">Dashboard</a>
                            <a href="" class="lj-btn">Logout</a>
                        </div>
                    <div>
                </div>    
    <?php
                break;
        }              
        return ob_get_clean();
}