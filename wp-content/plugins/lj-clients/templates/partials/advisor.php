<?php
/**
 * Advisor content
 */
function displayAdvisor()
{
    $userId     = get_current_user_id();
    $clientId   = getClientId( $userId );
    $advisorId  = get_post_meta( $clientId , 'client_advisor', true );
    $advisor    = getAdvisor( $advisorId ); //var_dump( $advisor );
    $phone      = extractAdvisorPhone( $advisor );
    $image      = extractImage( $advisor );
    $email      = extractEmail( $advisor );
    ob_start();

    ?>
        <div class="client-advisor" style="">
            <p><strong>Your Advisor is: </strong></p>
            <p><?php echo $advisor->first_name . ' ' . $advisor->last_name; ?></p>
            <div class="client-advisor_thumb">
                <img src="<?php echo $image; ?>" />
            </div>
            <p><?php echo $advisor->title; ?></p>
            <p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
            <p><?php echo $phone; ?></p>
        </div>
    <?php
        return ob_get_clean();
}