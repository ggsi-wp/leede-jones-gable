<?php

function form( $dAccounts, $mincount )
{
    ob_start();
	//var_dump($dAccounts); die;
	//var_dump($dAccounts);

	$arrClients = array();
	$arrAccount = array();

	if (count($dAccounts) != count($dAccounts, COUNT_RECURSIVE)) //array is multidimensional
	{
		foreach($dAccounts as $arrItem) {
			array_push($arrAccount, $arrItem[0]);
			array_push($arrClients, $arrItem[1]);
	  }
	}

	$arrClients = array_unique($arrClients);
	
	$clientsArray = array_filter($arrClients, function($a) {return $a !== "";});
	sort($clientsArray);	
	
	//var_dump($new);
	//echo "=========================";
	//var_dump($clientsArray); die;
    ?>

    <form method="post" action="" >
        <div>
            <label for="account-no">Account</label>
			<select name="account-no" id="account-no">
				<option <?php if($_POST['account-no'] == "0") { echo "selected=selected"; } ?> value="0">Choose a account</option>
                <?php 
					
					foreach($clientsArray as $clientItem)
					{
						$new = array_filter($dAccounts, function ($var) use ($clientItem) {
							return (strcasecmp($var[1], $clientItem) == 0);
						});

						$accountNumString = "";
						$tCount = 0;
						foreach($new as $accountItem)
						{
						
							if($tCount == 0)
							{
								$accountNumString = $accountItem[0];
							}
							else 
							{
								$accountNumString .= ',' . $accountItem[0];
							}

							$tCount++;
						}

						?>
						<option <?php if(strcasecmp($_POST['account-no'], $accountNumString) == 0) { echo "selected=selected"; } ?> value="<?php echo $accountNumString; ?>">Client - <?php echo $clientItem; ?></option>
						<?php
						 foreach($new as $accountItem)
						 {
						 	 ?>

							 <option <?php if(strcasecmp($_POST['account-no'], $accountItem[0]) == 0) { echo "selected=selected"; } ?> value="<?php echo $accountItem[0]; ?>">&nbsp;&nbsp;&nbsp;Account# <?php echo $accountItem[0]; ?></option>
							 <?php 

						 }
					}
				?>
            </select>


            
        </div>
        <div>
		<input type="radio" name="range" <?php if (isset($_POST['range']) == "" || $_POST['range']=="minCount") echo "checked";?> value="minCount">
		<!--<input type="radio" name="range" value="minCount" checked="checked"> -->
		Show <?php echo $mincount; ?> most recent</div>
        <div>
			<input type="radio" name="range" <?php if (isset($_POST['range']) && $_POST['range']=="days") echo "checked";?> value="days">
			<!-- <input type="radio" name="range" value="days" > -->
            <select name="days">
                <!--<option value="10">Last 10 days</option>
                <option value="30">Last 30 days</option>
                <option value="90">Last 3 months</option> -->
				<option <?php if($_POST['days'] == "10") { echo "selected=selected"; } ?> value="10">Last 10 days</option>
				<option <?php if($_POST['days'] == "30") { echo "selected=selected"; } ?> value="30">Last 30 days</option>
				<option <?php if($_POST['days'] == "90") { echo "selected=selected"; } ?> value="90">Last 3 months</option>
            </select>
        </div>
        <div>
			<input type="radio" name="range" <?php if (isset($_POST['range']) && $_POST['range']=="byMonth") echo "checked";?> value="byMonth">
			<!--<input type="radio" name="range" value="byMonth"> -->
            <select name="ljyear">
                <?php renderYearOptions( $yRange ); ?>
            </select>
            <select name="ljmonth">
                <?php renderMonthOptions(); ?>    
            </select>            
        </div>
        <div>
            <input type="submit" name="submit" value="Go" />
        </div>
    </form>
    <?php
    $o  = ob_get_clean();

    echo $o;
}    