<?php

function form( $dAccounts, $mincount )
{   
    ob_start();

	$arrClients = array();
	$arrAccount = array();

	if (count($dAccounts) != count($dAccounts, COUNT_RECURSIVE)) //array is multidimensional
	{
		foreach($dAccounts as $arrItem) {
			array_push($arrAccount, $arrItem[0]);
			array_push($arrClients, $arrItem[1]);
	  }
	}

	$arrClients = array_unique($arrClients);
	
	$clientsArray = array_filter($arrClients, function($a) {return $a !== "";});
	sort($clientsArray);

    ?>

    <form method="post" action="" >
        <div>
            <label for="account-no">Client</label>
            <select name="account-no" id="account-no">
                <?php renderAccountOptions( $clientsArray ); ?>
            </select>
        </div>
        <div>
		<!-- <input type="radio" name="range" value="minCount" checked="checked"> -->
		<input type="radio" name="range" <?php if (isset($_POST['range']) == "" || $_POST['range']=="minCount") echo "checked";?> value="minCount">
		Show <?php echo $mincount; ?> most recent</div>
        <div>
			<input type="radio" name="range" <?php if (isset($_POST['range']) && $_POST['range']=="byMonth") echo "checked";?> value="byMonth">
			<!-- <input type="radio" name="range" value="byMonth"> -->
            <select name="ljyear">
                <?php renderYearOptions( $yRange ); ?>
            </select>
            <select name="ljmonth">
                <?php renderMonthOptions(); ?>    
            </select>            
        </div>
        <div>
            <input type="submit" name="submit" value="Go" />
        </div>
    </form>
<?php
    $o  = ob_get_clean();

    echo $o;
}