<?php

function form( $dAccounts, $mincount )
{   
    ob_start();
	//var_dump($dAccounts); die;
	//var_dump($dAccounts);

	$arrClients = array();
	$arrAccount = array();

	if (count($dAccounts) != count($dAccounts, COUNT_RECURSIVE)) //array is multidimensional
	{
		foreach($dAccounts as $arrItem) {
			array_push($arrAccount, $arrItem[0]);
			array_push($arrClients, $arrItem[1]);
	  }
	}

	$arrClients = array_unique($arrClients);
	
	$clientsArray = array_filter($arrClients, function($a) {return $a !== "";});
	sort($clientsArray);
	
	//var_dump($arrAccount);
	//echo "===";
	//var_dump($clientsArray);
    ?>

    <form method="post" action="" >
		<div>
			 <label for="account-no">Client ID</label>
			<select name="account-no" id="account-no">
                <?php renderClientsOptions( $clientsArray ); ?>
            </select>
		</div>
        <div style="display:none;">
            <label for="account-no">Account</label>
            <select name="account-no-removed" id="account-no-removed">
                <?php renderAccountOptions( $arrAccount ); ?>
            </select>
        </div>
        <div>
			<input type="radio" name="range" <?php if (isset($_POST['range']) == "" || $_POST['range']=="minCount") echo "checked";?> value="minCount">
			
			Show <?php echo $mincount; ?> most recent</div>
        <div>
		<input type="radio" name="range" <?php if (isset($_POST['range']) && $_POST['range']=="byMonth") echo "checked";?> value="byMonth">
		
            <select name="ljyear">
                <?php renderYearOptions( $yRange ); ?>
            </select>
            <select name="ljmonth">
                <?php renderMonthOptions(); ?>    
            </select>            
        </div>
        <div>
            <input type="submit" name="submit" value="Go" />
        </div>
    </form>
<?php
    $o  = ob_get_clean();

    echo $o;
}