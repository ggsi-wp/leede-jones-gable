<?php
/**
 * Security questions functions
 */
namespace Security;
/*
function changePassword( $res = NULL )
{   
    ob_start(); 
    ?> 
        <style>
            form.lj-login-form {
                font-family: 'Roboto', sans-serif;
            }
            form.lj-login-form fieldset {
                margin: 0;
                padding: 0 0 1em 0;
                line-height: 2em;
                border: 0;
            }
            form.lj-login-form label {
                display: block;
                margin: 0;
                vertical-align: middle;
            }
            form.lj-login-form fieldset input {
                border: 1px solid #4774b3;
                background-color: #eee;
                color: #4774b3;
                box-shadow: none;
                border-radius: 5px;
                font-size: 18px;
                padding: 7px 5px;
                height: auto;
                max-width: 300px;
            }
            form.lj-login-form input[type="submit"] {
                padding: 12px 20px;
                border: 1px solid #3d6399;
                margin-top: 0;
                height: auto;
                font-family: 'Roboto', sans-serif;
                font-size: 18px;
                transition: all .25s ease-in-out;
                background: #3d6399;
                color: #fff;
                border-radius: 5px;
                cursor: pointer;                
            }



        </style>
        <form method="post" action="<?php echo site_url(); ?><?php echo $_SERVER['REQUEST_URI']; ?>" class="lj-login-form">
            <?php if( !empty( $res ) ): ?>
                <fieldset class="error">
                    <?php echo join( '<br />', $res ); ?>
                </fieldset>
            <?php endif; ?>

            <fieldset>
                <label for="new-password">New Password<span class="wpum-required">*</span></label>
                <div class="field required-field">
                    <input type="password" name="new-password" autocomplete="off" class="input-text" />
                </div>
            </fieldset>
            <fieldset>
                <label for="new-password-conf">Confirm New Password<span class="wpum-required">*</span></label>
                <div class="field required-field">
                    <input type="password" name="new-password-conf" autocomplete="off" class="input-text" />
                </div>
            </fieldset> 
    <?php
    $o  = ob_get_clean();  
    echo $o;
}

function endChange()
{
    ob_start(); ?>
            <!-- <input name="change-password" type="submit" class="button button-primary button-large" value="Save">-->
            <input type="submit" name="submit_login" class="button" value="Save">
        </form>
    <?php $o    = ob_get_clean();
    echo $o;
}*/

/**
 * Present the Security Questions for amendment and approval
 */
/*function changeSecurity( $questions, $cQuestions, $cAnswers, $qCount )
{   
    ob_start();  ?>
        <fieldset><?php echo __( 'For the security of your account, we require the answers to a few questions.' );
            for( $i=1; $i<=$qCount; $i++ ): 
                $qId    = array_shift( $cQuestions ); 
                $answer = array_shift( $cAnswers ); 
            ?>
                <p>
                    <label for="client_secq<?php echo $i; ?>">Question <?php echo $i; ?><span class="wpum-required">*</span></label>
                    <select name="client_secq<?php echo $i; ?>">
                        <?php getOptions( $questions, $qId ); ?>
                    </select>
                    <input type="text" name="client_secq<?php echo $i; ?>_ans" size=45 value="<?php echo $answer; ?>" />
                </p>
            <?php
            endfor; ?>

        </fieldset>
    <?php
        $o  = ob_get_clean();
        echo $o;
}*/


/*function askSecurity( $questions, $cQuestions )
{   
    $qId		= rand( 0, 2 ); 
    $question	= $questions[$cQuestions[$qId]-1]; 
    ob_start();
    ?>	
        <fieldset>
            <label><?php echo $question; ?> <span class="wpum-required">*</span></label>
            <div class="field required-field">
                <input type="text" class="input-text" name="answer" id="answer" placeholder="" value="" maxlength="" required="">
                <input type="hidden" name="qId" value="<?php echo $qId; ?>" />
            </div>
        </fieldset>
    <?php echo ob_get_clean();
}*/

/**
 * Link back to Login page
 */
function loginLink()
{
    ob_start(); ?>
        <p>You have successfully changed your password. <a href="<? echo site_url(); ?>/login">Click here to log in.</a>?</p>
    <?php
    $o  = ob_get_clean();
    echo $o;
}

/**
 * Verify responses 
 */
/*function verify()
{   
    $errors = [];
    $qCount     = esc_attr( get_option( 'security_questions_required' ) );

    if( empty( $_POST['new-password'] ) || empty( $_POST['new-password-conf'] ) )
    {
        $errors[]   = __( 'Both New Password and Confirm New Password must be completed' );
    }
    if( trim( $_POST['new-password'] ) !== trim( $_POST['new-password-conf'] )  )
    {
        $errors[]   = __( 'Confirm New Password must match New Password' );
    }
    if( isset( $_POST['client_secq1'] ) )
    {
        for( $i=1; $i<=$qCount; $i++ ): 
            if( empty( sanitize_text_field( $_POST['client_secq' . $i . '_ans'] ) ) )
            {
                $errors[]   = __( 'All questions must be answered' );
            }
            if( in_array( $_POST['client_secq' . $i], $questions ) )
            {
                $errors[]   = __( 'Security questions selected should be different' );
            }
            else
            {
                $questions[]    = $_POST['client_secq' . $i];
                $answers[]      = sanitize_text_field( $_POST['client_secq' . $i . '_ans'] );
            }
        endfor; 
    }
    if( isset( $_POST['answer'] ) )
    {
        if( empty( $answer = sanitize_text_field( $_POST['answer'] ) ) )
        {
            $errors[]   = __( "An answer is required" );
        }
        $userId     = filter_var( $_REQUEST['user_id'], FILTER_VALIDATE_INT );
        $clientId   = getClientId( $userId );
        $cAnswers   = json_decode( get_post_meta( $clientId, 'client_seqas', TRUE ) ); 
        $qId        = $_POST['qId']; 
        if( $answer !== $cAnswers[$qId] )
        {
            $errors[]   = __( "Your answer to the security question does not match the answer on record." );
        }
    }
    if( empty( $errors ) )
    {
        return TRUE;
    }
    else
    {

    }
    $o  = ( empty( $errors ) ) ? TRUE : $errors; 
    return $o;

}*/


/**
 * Update password
 */
function updatePassword( $userId, $clientId )
{   
    $password = sanitize_text_field( $_POST['new-password'] ); 
    // Update password
    wp_set_password( $password, $userId ); 
    unset( $_SESSION['pwChange'] );
    update_post_meta( $clientId, 'client_password_change', FALSE ); 
}

/**
 * Update answers provided
 */
function updateAnswers( $clientId )
{
    $qCount     = esc_attr( get_option( 'security_questions_required' ) );  // How many questions are needed
    $questions  = [];
    $answers    = [];

    for( $i=1; $i<=$qCount; $i++ ):
            $questions[]    = $_POST['client_secq' . $i];
            $answers[]      = sanitize_text_field( $_POST['client_secq' . $i . '_ans'] );
    endfor;
    update_post_meta( $clientId, 'client_secqs', json_encode( $questions ) );
    update_post_meta( $clientId, 'client_seqas', json_encode( $answers ) );    
    update_post_meta( $clientId, 'client_security_change', FALSE );
    unset( $_SESSION['secChange'] );
}