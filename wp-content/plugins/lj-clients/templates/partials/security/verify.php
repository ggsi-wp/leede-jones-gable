<?php
/**
 * Verify responses to password fields
 */

namespace Verify;

function verifyUpdate()
{
    $errors = [];
    /* if Session variable indicates password _must_ be reset, 
    * or if either New Password or Confirm Password field have entries, 
    * test responses
    */
    if( $_SESSION['secChange'] || $_SESSION['pwchange'] )
    {
        if( empty( $_POST['new-password'] ) )
        {
            $errors[]   = "New password is required";
        }
    }
    if( ( !empty( $_POST['new-password'] ) && $_POST['new-password'] !== $_POST['new-password-conf'] ) || ( !empty( $_POST['new-password-conf'] ) && $_POST['new-password'] !== $_POST['new-password-conf'] ) )
    {
        $errors[]   = __( 'Confirm New Password must match New Password' );
    }
    if( !empty( $password   = sanitize_text_field( $_POST['new-password'] ) ) )
    {
        if( !\Verify\testPassword( $password ) )
        {
            $errors[]   = "Password isn't strong enough. Please try again.";
        }
    }
    if( !isset( $_SESSION['pwchange'] ) || isset( $_SESSION['secChange'] ) )
    {
        // Test the questions, which are required
        $qCount     = esc_attr( get_option( 'security_questions_required' ) );  // How many questions are needed
        $questions  = [];
        $answers    = [];

        for( $i=1; $i<=$qCount; $i++ ):
            if( empty( sanitize_text_field( $_POST['client_secq' . $i . '_ans'] ) ) )
            {
                $errors[]   = $qCount . " questions are required";
            }
            if( in_array( $_POST['client_secq' . $i], $questions ) )
            {
                $errors[]   = 'Security questions selected should be different';
            }
            $questions[]    = $_POST['client_secq' . $i];
        endfor; 
        $errors = array_unique( $errors, SORT_STRING );
    }
    
    return empty( $errors ) ? TRUE : $errors;
}

/**
 * Verify that the reset password link is current and valid
 */
function verifyKey( $userId )
{   
    $userId     = filter_var( $_REQUEST['user_id'], FILTER_VALIDATE_INT );
    $userdata   = get_userdata( $userId );
    $key        = $_REQUEST['key'];
    $o          = check_password_reset_key( $key, $userdata->user_login ); 
    return $o;

}

/**
 * Verify that the fields are present and correct for resetting the password
 */
function verifyReset( $userId, $cQuestions, $cAnswers )
{   
    if( empty( $_POST['new-password'] ) || empty( $_POST['new-password-conf'] ) )
    {
        $errors[]   = __( 'Both New Password and Confirm New Password must be completed' );
    }
    if( $_POST['new-password'] !== $_POST['new-password-conf'] )
    {
        $errors[]   = __( 'Confirm New Password must match New Password' );
    }
    if( empty( $_POST['answer'] ) )
    {
        $errors[]   = __( "An answer is required" );
    }
    if( $_POST['answer'] != $cAnswers[$cQuestions[$_POST['qId']]-1] )
    {
        $errors[]   = __( "Your answer to the security question does not match the answer on record." );
    } 
    return empty( $errors) ? TRUE : $errors;

}

/**
 * Test that the new password is strong enough
 * [FIX] Parameters should be in Settings
 */
function testPassword( $password )
{
    // Validate password strength
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number    = preg_match('@[0-9]@', $password);
    $specialChars = preg_match('@[^\w]@', $password);

    if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) 
    {
        return FALSE;
    }
    
    return TRUE;
}