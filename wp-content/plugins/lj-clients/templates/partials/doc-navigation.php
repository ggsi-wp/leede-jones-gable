<?php
/**
 * Document navigation section for Client Dashboard
 */
?>
<section>
    <?php if( $doxim ): ?>
        <h3>My Documents</h3>
        <p><a href="<?php echo site_url(); ?>/doxim-documents-page/?tab=trade">Trade Confirms</a></p>
        <p></p>
        <p><a href="<?php echo site_url(); ?>/doxim-documents-page/?tab=account">Monthly Statements</a></p>
        <p></p>
        <p><a href="<?php echo site_url(); ?>/doxim-documents-page/?tab=tax">Tax Documents</a></p>
        <p></p>
        <h3>Information & Resources</h3>
        <p><a href="<?php echo site_url(); ?>/doxim-documents-page/?tab=reports">Quarterly Reports</a></p>
        <p>Read Quarterly reports from the various companies you have holdings for.</p>          
    <?php endif; ?>
    <?php if( $broadridge ): ?>
        <h3>My Accounts</h3>
        <p><a href="">Current Balances</a></p>
        <p>This is what your current...</p>  
    <?php endif; ?>  
  

</section>
