<?php
/**
 * Change Questions form
 */
?>
<h2>Confirm Security Questions & Answers</h2>

<form action="<?php echo site_url(); ?>/manage-account" method="post">
    <?php if( !empty( $passErrors ) ): ?>
        <p class="error"><?php echo reset( $passErrors ); ?></p>
    <?php endif; ?>
    <?php if( !empty( $passMessages ) ): ?>
        <p class="message"><?php echo reset( $passMessages ); ?></p>
    <?php endif; ?>    

    <input name="change-questions" type="submit" class="button button-primary button-large" value="Save Security Questions">
</form>