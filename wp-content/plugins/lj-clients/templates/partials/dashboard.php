<?php
/**
 * Template for Client Dashboard
 */

function dashboard()
{
    $userId = get_current_user_id();
    // Need to get clientId where 'client_user_id' == $userId;
    $clientId   = getClientId( $userId );
    $broadridge = esc_attr( get_post_meta( $clientId, 'client_broadridge', TRUE ) );
    $doxim      = esc_attr( get_post_meta( $clientId, 'client_doxim', TRUE ) ); 

    ob_start();
    ?>
        <?php //include_once( 'partials/advisor.php' ); ?>

        <?php include_once( 'partials/welcome.php' ); ?>
        <?php //include_once( 'partials/dashboard-posts.php' ); ?>
        <?php //include_once( 'partials/doc-navigation.php' );
    $o  = ob_get_clean();

    return $o;
}