<?php

function displayWelcome() {
	$firstname  = get_post_meta( $_SESSION['clientId'], 'client_firstname', TRUE );
	$wel = '';

	$wel .= '<div class="welcome-wrap">';
	$wel .= '<h2>Client Dashboard</h2>';
	$wel .= '<p>Welcome back, '.$firstname.'</p>';
	$wel .= '<a href="/client-advisor/">Contact your advisor</a>';
	//$wel .= '<p></p>';
	$wel .= '<br /><a href="/change-password/">Manage your security details</a>';
	$wel .= '</div>';

	return $wel;

}

?>