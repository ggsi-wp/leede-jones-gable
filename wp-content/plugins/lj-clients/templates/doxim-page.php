<?php
/**
 * Doxim page generation
 */
namespace LJClient\Doxim; 

function displayDoximPage( $atts )
{   
    include_once( INCLUDES . 'doxim.class.php' );
    include_once( INCLUDES . 'form-builder.php' );

    $bin    = get_option( 'doxim_bin' ); 
    $pass   = get_option( 'doxim_password' ); 
    $uri    = get_option( 'doxim_apiurl' ); 

    $doxim = new \Doxim( $bin, $pass, $uri );
	
    $dAccounts  = unserialize( get_post_meta( $_SESSION['clientId'], 'client_doxim_accts', TRUE ) ); //var_dump( $dAccounts );

	$originalAccount = $dAccounts;
    $dateStr    = '';
    $minCount   = $atts['mincount']; 
	
	//check page type
	$arrClients = array();
	$arrAccount = array();

	if (count($dAccounts) != count($dAccounts, COUNT_RECURSIVE)) //array is multidimensional
	{
		foreach($dAccounts as $arrItem) {
			array_push($arrAccount, $arrItem[0]);
			array_push($arrClients, $arrItem[1]);
	  }
	}

	$arrClients = array_unique($arrClients);
	
	$clientsArray = array_filter($arrClients, function($a) {return $a !== "";});
	sort($clientsArray);

	if($atts['type'] != "trade")
	{
		$dAccounts = $clientsArray;
	}
	else
	{
		$dAccounts = $arrAccount;
	}

	//end check page type
	$tempResultAccounts = $dAccounts;
	

    if( isset( $_POST['submit'] ) )
    {	
		$postArray = array();

		$searchForValue = ',';
		$stringValue = $_POST['account-no'];
		if( strpos($stringValue, $searchForValue) !== false ) {
			 $postArray = explode (",", $stringValue);
		}
		else
		{
			$postArray = [$_POST['account-no']];
		}
		
        $tempResultAccounts  = ( $_POST['account-no'] < 0 ) ? $dAccounts : $postArray;
        $dateStr    = \LJClient\Doxim\createDateStr(); 
    }
	
	//$dateStr = "&dates=01/03/2019-31/3/2019";
    //var_dump( $dateStr ); 

    $results    = $doxim->getResults( $tempResultAccounts, $atts['type'], $dateStr, $atts['viewid'], $minCount ); //var_dump( $results );
	
	//var_dump( $results );

    include_once( DOXIMFORM . 'form-' . $atts['type'] . '.php'  );
    include_once( DOXIMFORM . 'result-' . $atts['type'] . '.php'  );

    ob_start(); ?>
        <div class="doxim-form">
            <?php echo form( $originalAccount, $atts['mincount'] ); ?>
            <?php echo result( $results ); ?>

        </div>
    <?php
        return ob_get_clean();
}

/**
 *  Create the date string based on the form selections
 */
function createDateStr()
{
    $pre    = '&dates='; 
    switch( $_POST['range'] )
    {
        case 'minCount':
            $o  =  '';
            break;
        case 'byMonth':
            $year   = sanitize_text_field( $_POST['ljyear'] );
            $month  = sanitize_text_field( $_POST['ljmonth'] );
			
            if( 0 == $month )
            {
                $o  = $pre . '01/01/' . $year . '-31/12/' . $year;
            }
            else
            {
				//$month = $month + 1;
                $o  =  $pre . '01/' . str_pad( $month, 2, '0', STR_PAD_LEFT ) . '/' . $year . '-' . cal_days_in_month( CAL_GREGORIAN, $month, $year ) . '/' . $month . '/' . $year;
            }   
            break;
        case 'days':
            $days   = sanitize_text_field( $_POST['days'] );
            $o      = $pre . date( 'd/m/Y' ) .  '-' . date( 'd/m/Y', strtotime( "-" . $days . "day" ) ); 
            break;
        case 'byYear':
            $year   = sanitize_text_field( $_POST['ljyear'] );    
            $o      = $pre . '01/01/' . $year . '-31/12/' . $year;
            break;
        case 'byTaxYear':
            $year   = sanitize_text_field( $_POST['tax-year'] );    
            $o      = $pre . '01/01/' . $year . '-31/12/' . $year;
            break;
    }
    return $o;
}

