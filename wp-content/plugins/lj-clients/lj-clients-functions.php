<?php 
/**
 * Functions for Client admin page
 */

 /**
 * Generate Update button if not new post
 */
function getUpdateButton()
{   
    global $post;
    $url    = $_SERVER['REQUEST_URI'];
    if( FALSE !== strpos( $url, 'post-new' ) ) return '';
    if( 'draft' == $post->post_status || 'auto-draft' == $post->post_status ) return '';
    ?>
        <div>
            <span class="spinner"></span>
            <input name="original_publish" type="hidden" id="original_publish" value="Update">
            <input name="save" type="submit" class="button button-primary button-large" value="Update">
		</div> 
    <?php
}

/**
 * Get Advisors
 */
function getAdvisors()
{
    global $wpdb;

    $sql    = "SELECT * FROM " . $wpdb->prefix . "connections ORDER BY last_name, first_name";
    $res    = $wpdb->get_results( $sql ); 
    $o      = [];
    foreach( $res as $r )
    {
        $o[$r->id]  = $r->first_name . ' ' . $r->last_name;
    }
    
    return $o;
}

/**
 * Get Advisor
 */
function getAdvisor( $advisorId )
{
    global $wpdb;

    $sql    = "SELECT * from " . $wpdb->prefix . "connections WHERE id = $advisorId"; 

    return $wpdb->get_row( $sql );

}

/**
 * Fetch Advisor
 */
function fetchAdvisor( $clientId )
{
    $advisorId  = get_post_meta( $clientId, 'client_advisor', TRUE );
    $advisor    = getAdvisor( $advisorId );
    return $advisor->first_name . ' ' . $advisor->last_name;
}

/**
 * Security Question Options
 */
function getOptions( $questions, $qId = NULL )
{
    foreach( $questions as $k => $v ): ?>
        <option value="<?php echo $k+1; ?>" <?php echo $k+1 == $qId ? 'selected="selected"' : ''; ?>><?php echo $v; ?></option>
    <?php endforeach;
}

/**
 * Get Security Questions
 */
function getQuestions()
{
    $o  = [];
    $count  = 0;
    $flag   = TRUE;
    while( $flag )
    {
        $str    = 'security_questions_' . ( $count+1 ); 
        if( FALSE == ( $q  = esc_attr( get_option( $str ) ) ) ) break;
        $count++;
        $o[]    = $q;
    } 
    return $o;
}

/**
 * Get Security Questions answered by Client
 */
function getClientQuestions( $post_id )
{
    $o  = [];
    if( !empty( $post_id ) )
    {
        $o  = json_decode( get_post_meta( $post_id, 'client_secqs', TRUE ) );
    } 
    return $o;
}
/**
 * Get Answers to security questions answered by Client
 */
function getClientAnswers( $post_id )
{
    $o  = [];
    if( !empty( $post_id ) )
    {
        $o  = json_decode( get_post_meta( $post_id, 'client_seqas', TRUE ) );
    }
    return $o;
}

/**
 * Encrypt string
 */
function gsEncrypt( $string )
{
    $cypherMethod   = 'AES-256-CBC';
    $ivlength       = openssl_cipher_iv_length($cypherMethod);
    $encrypted  = openssl_encrypt( $string, $cypherMethod, AUTH_KEY, $options=0, substr( AUTH_SALT, 0, $ivlength ) );
    return $encrypted;
}

/**
 *  Set error messages
 */
function setError( $error )
{
    $msg    = unserialize( get_transient( 'jl_client_error' ) ); 
    if( empty( $msg ) ) $msg    = [];
    array_unshift( $msg, $error ); 
    set_transient( 'jl_client_error', $msg, 45 );
}
/**
 * Display either email input field or provided email
 */
function displayEmail()
{
    global $post; 
    $url    = $_SERVER['REQUEST_URI']; 
    if( FALSE === strpos( $url, 'post-new' ) && 'auto-draft' != $post->post_status && 'draft' != $post->post_status )
    {
        echo ( $email = get_post_meta( get_the_ID(), 'client_email', true ) );
        ?> <input type="hidden" name="client_email" value="<?php echo $email; ?>" /> <?php
    }
    else
    { ?>
        <input type="email" name="client_email" placeholder="Client's email" value="<?php echo get_post_meta( get_the_ID(), 'client_email', true ); ?>"/>This is the client's username also
    <?php }

}

/**
 * Get ClientId from UserId
 */
function getClientId( $userId )
{   
    $args   = [
        'post_type'     => 'client',
        'meta_key'      => 'client_user_id',
        'meta_value'    => $userId
    ];
    $query  = new WP_Query( $args ); //var_dump( $query->request );
    return $query->posts[0]->ID;
}

/**
 * Add content to Client Centre menu
 */
function amend_menus( $items, $args )
{   
    if( 'client-centre' == $args->menu->slug )
    {
        $userId     = get_current_user_id();
        $clientId   = getClientId( $userId );
        $advisorId  = get_post_meta( $clientId , 'client_advisor', true );
        $advisor    = getAdvisor( $advisorId ); //var_dump( $advisor );
        $phone      = extractAdvisorPhone( $advisor );
        $image      = extractImage( $advisor );
        $email      = extractEmail( $advisor );
        $doxim      = get_post_meta( $clientId, 'client_doxim', TRUE );
        $broadridge = get_post_meta( $clientId, 'client_broadridge', TRUE );

        ob_start();
        ?>
            <?php if( $doxim )
                { ?>
                    <p><strong>Your Documents</strong></p>
                    <div style="padding-left:15px;">
                        <?php echo $items; ?>
                    </div>
                <?php } ?>
            <?php if( $broadridge ): ?>
                    <p><strong>Your Accounts</strong></p>
                    <div style="padding-left:15px;">
                        <a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=index.htm">Broadridge Reports</a>
                        <a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=index-fr.htm">Reports Broadridge (fr)</a>
                    </div>
            <?php endif; ?>
            <hr>
            <!--<li><a href="<?php echo wp_logout_url(); ?>" class="btn">Log Out</a></li>-->
            <li class="client-advisor" style="background-color:#ccc;padding:10px;">
                <p><strong>Your Advisor is: </strong></p>
                <p><?php echo $advisor->first_name . ' ' . $advisor->last_name; ?></p>
                <div class="client-advisor_thumb">
                    <img src="<?php echo $image; ?>" />
                </div>
                <p><?php echo $advisor->title; ?></p>
                <p><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
                <p><?php echo $phone; ?></p>
            </li>
        <?php 
            $o  = ob_get_clean();
            return $o;
    }
    return $items;
}

function extractAdvisorPhone( $advisor )
{
    $phone  = unserialize( $advisor->phone_numbers );
    foreach( $phone as $p )
    {
        if( 'public' == $p['visibility'] ) return $p['number'];
    }
    return NULL;
}
function extractImage( $advisor )
{
    $options    = json_decode( $advisor->options ); //var_dump( $options->image->meta );
    return $options->image->meta->original->url;
}
function extractEmail( $advisor)
{
    $email  = unserialize( $advisor->email ); 
    foreach( $email as $e )
    {
        if( 'public' == $e['visibility'] ) return $e['address'];
    }
    return NULL;
}
