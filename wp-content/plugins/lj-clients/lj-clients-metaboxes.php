<?php
/**
 * Meta boxes for Client admin page
 */

add_action( 'add_meta_boxes', 'lj1240_register_meta_boxes' );


 /**
 * Register meta boxes
 */
function lj1240_register_meta_boxes()
{
    add_meta_box( 'jl_agreement', __( 'Client Agreement Status' ), 'lj1240_agreement_callback', 'client', 'normal','high' );
    add_meta_box( 'jl_contact', __( 'Credentials' ), 'lj1240_contact_callback', 'client', 'normal','high' );
    add_meta_box( 'jl_security', __( 'Client Security Questions' ), 'lj1240_security_callback', 'client', 'normal','high' );    
    add_meta_box( 'jl_broadridge', __( 'Broadridge' ), 'lj1240_broadridge_callback', 'client', 'normal','high' );  
    add_meta_box( 'jl_doxim', __( 'Doxim' ), 'lj1240_doxim_callback', 'client', 'normal','high' );  

}

/**
 * Meta box agreement form callback
 */
function lj1240_agreement_callback()
{ 
    if( $error  = get_transient( 'jl_client_error' ) )
    {   
        
        ?>
        <div class="error">
            <?php foreach( $error as $e ): ?>
                <p><?php echo $e; ?></p>
            <?php endforeach; ?>
        </div>
        <?php
    } 

    $pwChange   = esc_attr( get_post_meta( get_the_ID(), 'client_password_change', TRUE ) );
    $ljAgreement= esc_attr( get_post_meta( get_the_ID(), 'client_lj_agreement', TRUE ) );
    $bAgreement = esc_attr( get_post_meta( get_the_ID(), 'client_bb_agreement', TRUE ) );
    ?>
    <p>
        <label><?php echo __( 'Does client need to change password on next login?' ); ?></label>
        <input type="radio" name="client_password_change" value="0" <?php echo empty( $pwChange ) ? 'checked' : ''; ?>/>No
        <input type="radio" name="client_password_change" value="1" <?php echo !empty( $pwChange ) ? 'checked' : ''; ?>/>Yes
    </p>
    <p>
        <label><?php echo __( 'Needs to agree to Leede Jones Agreement?' ); ?></label>
        <input type="radio" name="client_lj_agreement" value="0" <?php echo 0 == $ljAgreement ? 'checked' : ''; ?>/>No
        <input type="radio" name="client_lj_agreement" value="1" <?php echo 1 == $ljAgreement ? 'checked' : ''; ?>/>Yes
    </p>
    <p>
        <label><?php echo __( 'Needs to agree to Broadbridge Agreement?' ); ?></label>
        <input type="radio" name="client_bb_agreement" value="0" <?php echo 0 == $bAgreement ? 'checked' : ''; ?>/>No
        <input type="radio" name="client_bb_agreement" value="1" <?php echo 1 == $bAgreement ? 'checked' : ''; ?>/>Yes
    </p>        
    <?php
}

/**
 *  Meta box contact details callback
 */
function lj1240_contact_callback()
{ ?>
    <p>
        <label><?php echo __( 'Username' ); ?></label>
        <?php displayEmail(); ?>
    </p>
    <p>
        <label><?php echo __( 'Password' ); ?></label>
        <input type="password" name="client_new_pass" /><br />  
        <label><?php echo __( 'Confirm' ); ?></label>
        <input type="password" name="client_new_pass_conf" /><br />
        * Client will reset password on next login   
        <?php getUpdateButton(); ?>
    </p>    
    <p>
        <label for="client_firstname">First Name</label>
        <input id="client_firstname" type="text" name="client_firstname" placeholder="First Name" value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'client_firstname', true ) ); ?>"/>
    </p>
    <p>
        <label for="client_lastname">Last Name</label>
        <input id="client_lastname" type="text" name="client_lastname" placeholder="Last Name" value="<?php echo esc_attr( get_post_meta( get_the_ID(), 'client_lastname', true ) ); ?>" />
    </p>    
    <p>
        <label for="client_advisor">Advisor</label>
        <?php $advisors = getAdvisors(); 
            $advisorId  = esc_attr( get_post_meta( get_the_ID(), 'client_advisor', true ) );
        ?>
        <select name="client_advisor">
            <?php foreach( $advisors as $k => $a ): ?>
                <option value="<?php echo $k; ?>" <?php echo $k == $advisorId ? 'selected' : ''; ?>><?php echo $a; ?></option>
            <?php endforeach; ?>
        </select>
    </p>     
    <?php
}

/**
 * Meta box security questions callback
 */
function lj1240_security_callback()
{  
    $post_id    = isset( $_GET['post'] ) ? $_GET['post']: NULL;
    $questions  = getQuestions();
    $qCount     = esc_attr( get_option( 'security_questions_required' ) );
    $Cquestions = getClientQuestions( $post_id ); 
    $Canswers   = getClientAnswers( $post_id );
    echo '<p>Minimum ' . $qCount . ' question' . ( $qCount > 0 ? 's' : '' ) . ' will be required from the Client</p>'; 

    for( $i=1; $i<=$qCount; $i++ ): 
        $qId    = array_shift( $Cquestions ); 
        $answer = array_shift( $Canswers ); 
    ?>
        <p>
            <label for="client_secq<?php echo $i; ?>"></label>
            <select name="client_secq<?php echo $i; ?>">
                <?php getOptions( $questions, $qId ); ?>
            </select>
            <input type="text" name="client_secq<?php echo $i; ?>_ans" size=45 value="<?php echo $answer; ?>" />
        </p>
    <?php
        endfor;
    echo '<p>Will be verified on next login</p>';
}

/**
 * Meta box Broadridge questions callback
 */
function lj1240_broadridge_callback()
{   
    $post_id    = isset( $_GET['post'] ) ? $_GET['post']: NULL;
    $broadridge = esc_attr( get_post_meta( $post_id, 'client_broadridge', true ) ); 
    $broadridge_username    = esc_attr( get_post_meta( $post_id, 'client_broadridge_username', true ) ); 
    $broadridge_password    = esc_attr( get_post_meta( $post_id, 'client_broadridge_password', true ) );
    ?>
        <p>
            <label><?php //echo __( 'Has Broadridge account?' ); ?></label>
            <input type="radio" name="client_broadridge" value="0" <?php echo 0 == $broadridge ? 'checked' : ''; ?>/>Off
            <input type="radio" name="client_broadridge" value="1" <?php echo 1 == $broadridge ? 'checked' : ''; ?>/>On
        </p>
        <p>
            <label for="client_broadridge_username">Username</label>
            <input id="client_broadridge_username" type="input" name="client_broadridge_username" placeholder="Broadridge Username" value="<?php echo $broadridge_username; ?>" />
        </p>
        <p>
            <label for="client_broadridge_password">Password</label>
            <input id="client_broadridge_password" type="password" name="client_broadridge_password" placeholder="Broadridge Password" value="<?php echo $broadridge_password; ?>"/> 
            <?php echo ( !empty( $broadridge_password) ? '(Password is set)' : '' ); ?> 
        </p>    
    <?php
}

/**
 * Meta box Doxim accounts callback
 */
function lj1240_doxim_callback()
{   
    $post_id    = isset( $_GET['post'] ) ? $_GET['post'] : NULL;
    $doxim    = esc_attr( get_post_meta( $post_id, 'client_doxim', true ) );
    ?>
        <p>
            <label><?php //echo __( 'Access to Doxim Files' ); ?></label>
            <input type="radio" name="client_doxim" value="0" <?php echo 0 == $doxim ? 'checked' : ''; ?> />Off
            <input type="radio" name="client_doxim" value="1" <?php echo 1 == $doxim ? 'checked' : ''; ?> />On
        </p>
        <p>
			<label for="client_doxim_account">Add Client ID</label>
			<input id="client_doxim_account_client_id"  type="text" name="client_doxim_account_client_id" placeholder="Doxim Client ID" />
			&nbsp;&nbsp;
            <label for="client_doxim_account">Add Account</label>
            <input id="client_doxim_account" type="text" name="client_doxim_account" placeholder="Doxim Account" />
			
            <?php getUpdateButton(); ?>
            <?php 
                $dAccounts    = unserialize( get_post_meta( $post_id, 'client_doxim_accts', true ) );
				//var_dump($dAccounts); 
                if( !empty( $dAccounts ) )
                {   ?>
                    <table>
                        <thead>
                            <tr>
								<td>Client ID</td>
								<td>&nbsp;&nbsp;&nbsp;</td>
                                <td>Account Number</td>
                                <td>Remove</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
								$tempAccountTwoD = array();
								if (count($dAccounts) == count($dAccounts, COUNT_RECURSIVE)) 
								{
								  //echo 'array is not multidimensional';
								  foreach($dAccounts as $string) {
										$values = array();
										 array_push($values, $string);
										 array_push($values, '');
										 array_push($tempAccountTwoD, $values);
								  }
								  $dAccounts = $tempAccountTwoD;
								}
								//var_dump($dAccounts); die;
								$itemCount =0;
                                foreach( $dAccounts as $acct ): ?>
                                    <tr>
										<td><?php echo $acct[1] ?></td>
										<td>&nbsp;&nbsp;&nbsp;</td>
                                        <td><?php echo $acct[0] ?></td>
                                        <td><input type="checkbox" name="remove_doxim_account[]" value="<?php echo $itemCount; ?>" />
                                    </tr>
                            <?php
								$itemCount++;
                                endforeach; ?>
                        </tbody>
                    </table>   
                    <?php
                } ?>
        </p>        

    <?php
}

/**
 * Save Meta box information
 */
function jl1240_save_meta_box( $post_id )
{
    global $post;

    if ( $parent_id = wp_is_post_revision( $post_id ) ) {
        $post_id = $parent_id;
    }

    $fields = [
        'client_lj_agreement',
        'client_bb_agreement',
        'client_email',
        'client_firstname',
        'client_lastname',
        'client_advisor',
/*        'client_secq1',
        'client_secq2',
        'client_secq3',
        'client_secq1_ans',
        'client_secq2_ans',
        'client_secq3_ans'*/
    //    'client_broadridge',
    //    'client_doxim'
    ];

    

    $user_id = get_post_meta( get_the_ID(), 'client_user_id', true );
    if( empty( $user_id ) )
    {
        $user_id = createClient();
        update_post_meta( $post_id, 'client_user_id', $user_id );
        update_post_meta( $post_id, 'client_security_change', TRUE );
    } 
    if( isset( $_POST['client_new_pass'] ) && !empty( $password = sanitize_text_field( $_POST['client_new_pass'] ) ) )
    {   
        // Update password
        wp_set_password( $password, $user_id );
        // Client will need to change password on next login
        update_post_meta( $post_id, 'client_password_change', TRUE );
    }

    // Test Security Questions - need answers

    $qCount     = esc_attr( get_option( 'security_questions_required' ) );  // How many questions are needed
    $questions  = [];
    $answers    = [];

    for( $i=1; $i<=$qCount; $i++ ):
        if( empty( sanitize_text_field( $_POST['client_secq' . $i . '_ans'] ) ) ) continue;
        if( in_array( $_POST['client_secq' . $i], $questions ) )
        {
            setError( 'Security questions selected should be different' );
        }
        else
        {
            $questions[]    = $_POST['client_secq' . $i];
            $answers[]      = sanitize_text_field( $_POST['client_secq' . $i . '_ans'] );
        }

    endfor; 
    update_post_meta( $post_id, 'client_secqs', json_encode( $questions ) );
    update_post_meta( $post_id, 'client_seqas', json_encode( $answers ) );


    // Test Broadridge Information
    if( empty( $_POST['client_broadridge_username'] ) && empty( $_POST['client_broadridge_password'] ) )
    {
        update_post_meta( $post_id, 'client_broadridge', 0 );
    }
    elseif( ( !empty( $_POST['client_broadridge_username'] ) && empty( $_POST['client_broadridge_password'] ) ) || ( empty( $_POST['client_broadridge_username'] ) && !empty( $_POST['client_broadridge_password'] ) ) )
    {
        setError( 'Broadridge Account requires both Broadridge Username and Broadridge Password' );
        update_post_meta( $post_id, 'client_broadridge', 0 );
    }
    else
    {
        update_post_meta( $post_id, 'client_broadridge_username', sanitize_text_field( $_POST['client_broadridge_username'] ) );
        update_post_meta( $post_id, 'client_broadridge_password', sanitize_text_field( $_POST['client_broadridge_password'] ) ); //gsEncrypt( sanitize_text_field( $_POST['client_broadridge_password'] ) ) );
        update_post_meta( $post_id, 'client_broadridge', 1 );
    }

    // Test Doxim Account Information

    // Check if there are any existing Doxim accounts
    $dAccounts  = unserialize( get_post_meta( get_the_ID(), 'client_doxim_accts', true ) ); 
	$dAccountTwoD = array();
	if (count($dAccounts) == count($dAccounts, COUNT_RECURSIVE)) 
	{
	  //echo 'array is not multidimensional';
	  foreach($dAccounts as $string) {
			$values = array();
			 array_push($values, $string);
			 array_push($values, '');
			 array_push($dAccountTwoD, $values);
	  }
	}
	else
	{
	  //echo 'array is multidimensional <br><br>';
	  $dAccountTwoD = $dAccounts;
	}
	

	//print_r(get_post_meta( get_the_ID(), 'client_doxim_accts', true ));
	

    if( empty( $dAccounts ) )  $dAccounts = [];
	//client_doxim_account_client_id
	//print_r(sanitize_text_field( $_POST['client_doxim_account_client_id'] ));
	//print_r(sanitize_text_field( $_POST['client_doxim_account'] )); die;

    if( !empty( $newAccount = sanitize_text_field( $_POST['client_doxim_account'] ) ) || !empty( $newAccount = sanitize_text_field( $_POST['client_doxim_account_client_id'] ) ))
    {
        array_push( $dAccounts, sanitize_text_field( $_POST['client_doxim_account'] ) );
		
		$tempArr = array();
		array_push($tempArr, sanitize_text_field( $_POST['client_doxim_account'] ));
		array_push($tempArr, sanitize_text_field( $_POST['client_doxim_account_client_id'] ));
		array_push($dAccountTwoD, $tempArr);
    } 
	//var_dump($dAccountTwoD); die;
	//var_dump($_POST['remove_doxim_account']);
    if( isset( $_POST['remove_doxim_account'] ) )
    {
		$removeItems = $_POST['remove_doxim_account'];
		$removedArray = array();

		$tempCount = 0;
		foreach($dAccountTwoD as $item)
		{
			if(in_array($tempCount, $removeItems))
			{
				array_push($removedArray, $item);
			}

			
			$tempCount++;
		}

		foreach($dAccountTwoD as $idx => $row) {
			if(in_array($row, $removedArray))
			{
				unset($dAccountTwoD[$idx]);
			}
			//echo "KK==={";
			//var_dump($row);
			//echo "}===||";
		}

		
		//var_dump($dAccountTwoD);
		//  = array_diff( $dAccountTwoD, $removedArray); 
		//echo "============";
		//var_dump($dResult); 
		//die;

		//print_r( $_POST['remove_doxim_account']);

		//$tempRemove = array();
		//array_push($tempRemove, "44444", "333");
		//$dAccountTwoD  = array_diff( $dAccounts, $tempRemove ); 
        //$dAccountTwoD  = array_diff_assoc( $dAccounts, $_POST['remove_doxim_account'] ); 
		//echo "==> ";
		//unset($dAccountTwoD[2]);
		//var_dump($dAccountTwoD); 
		//die;
    }
    if( empty( $dAccountTwoD ) )
    {
        update_post_meta( $post_id, 'client_doxim', 0 );
    }
    else
    {   
        update_post_meta( $post_id, 'client_doxim', 1 );
    } 
    update_post_meta( $post_id, 'client_doxim_accts', serialize( $dAccountTwoD ) );

    // If new Client or password reset, send email to Client

    foreach ( $fields as $field ) {
        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, sanitize_text_field( $_POST[$field] ) );
        }
    } 

}
