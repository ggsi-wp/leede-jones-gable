<?php
/**
 * Custom pages for LJ Clients
 */


/**
 * Register custom page
 */
function add_custom_pages()
{   
    $my_post    = [
        'post_title'    => wp_strip_all_tags( 'Client Dashboard' ),
        'post_content'  => '[lj-client-restriction][lj-client-dashboard]',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type'     => 'page'
    ];

    wp_insert_post( $my_post );

    $my_post    = [
        'post_title'    => wp_strip_all_tags( 'Manage Account' ),
        'post_content'  => '[lj-client-restriction][lj-client-manage-account]',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type'     => 'page'
    ];

    wp_insert_post( $my_post );  
    
    $my_post    = [
        'post_title'    => wp_strip_all_tags( 'Change Password' ),
        'post_content'  => '[lj-client-restriction][lj-client-change-password]',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type'     => 'page'
    ];

    wp_insert_post( $my_post );     

    $my_post    = [
        'post_title'    => wp_strip_all_tags( 'Client Advisor' ),
        'post_content'  => '[lj-client-restriction][lj-client-advisor]',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type'     => 'page'
    ];

    wp_insert_post( $my_post );        
    
}    

function remove_custom_pages()
{
    $page    = get_page_by_title( 'Client Dashboard' );
    wp_delete_post( $page->ID );
    $page    = get_page_by_title( 'Manage Account' );
    wp_delete_post( $page->ID );
    $page    = get_page_by_title( 'Change Password' );
    wp_delete_post( $page->ID );    
    $page    = get_page_by_title( 'Client Advisor' );
    wp_delete_post( $page->ID );     

}