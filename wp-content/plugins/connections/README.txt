=== Connections Business Directory ===
Contributors: shazahm1@hotmail.com
Donate link: https://connections-pro.com/
Tags: address book, business directory, chamber of commerce, church directory, company directory, contact directory, directory, listings directory, local business directory, link directory, member directory, staff directory
Requires at least: 4.7.12
Tested up to: 5.2
Requires PHP: 5.6.20
Stable tag: 8.43
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

An easy to use directory plugin to create an addressbook, business directory, member directory, staff directory or church directory.

== Description ==

[Showcase](https://connections-pro.com/showcase/) | [Documentation](https://connections-pro.com/documentation/contents/) | [Support](https://connections-pro.com/support/) | [Templates](https://connections-pro.com/templates/) | [Extensions](https://connections-pro.com/extensions/)

Quite simply, Connections Business Directory is one of the [best business directory plugins available](https://wordpress.org/support/plugin/connections/reviews/?filter=5) for WordPress. Its simplicity in design and function, vast array of unique features and versatility are the reasons more and more people are turning to Connections Business Directory for their directory needs. You can use Connections to create a simple address book, maintain a staff or member directory and run a business directory or link directory. Connections Business Directory was built bottom up to be as configurable as possible while providing the features you need. Read on to learn about some of the best features Connections Business Directory has to offer...

= Features =

* Compatible with all themes including popular themes such as Avada, Divi, Enfold and OceanWP.
* Compatible with most popular page builders such as Elementor, Beaver Builder, Brizy, Visual Composer, Fusion, SiteOrigin, etc...
* Supports the WordPress Block (Gutenberg) Editor with the following blocks.
    * [Directory Block](https://connections-pro.com/documentation/block/directory/) :: Use this block to embed your directory in a page.
    * [Team Block](https://connections-pro.com/documentation/block/team/) :: It's easy to use Connections to build your team page! Team Block is a full featured block with multiple layout styles. While premium team plugins can set you back $20 or more, Team Block is free for all Connections users.
    * [Upcoming List Block](https://connections-pro.com/documentation/block/upcoming-list/) :: Use this block to display an upcoming list of anniversaries, birthdays or other events.
* **[Highly rated support.](https://wordpress.org/support/plugin/connections/reviews/?filter=5)**
* [Continuously updated](https://wordpress.org/plugins/connections/#developers) bringing you new features for free.
* Dashboard admin page where you can see at a glance today's anniversaries and birthdays as well as upcoming anniversaries and birthdays of members in your directory.
* Multiple entry types from which to choose; such as individual, organization and family. The family entry type is unique to Connections. This entry type allows you to group individuals together as a family which makes Connections ideally suited for creating a church directory.
* You control which entries are viewable to the public and which entries are private, viewable for logged in users only. You can even have entries set as unlisted so only admins can view them.
* **[Repeatable fields](https://connections-pro.com/documentation/add-entry/#Repeatable_Fields)** for address, phone numbers, email, IM, social media, links and dates which allow you to add as much or as little as you need for each business in your business directory.
* Hierarchical (nested) **category support**. Businesses and members in your business directory can be added to any number of categories.
* Include a biographical text for an individual member or business description for each entry in your business directory using an easy to use rich text editor.
* You can easily add a business logo or photo of the staff member for each entry in your business directory or staff directory. The photo and logo images are fully responsive with **HiDPI (Retina) display support**.
* **Scalable,** manage directories which [contain hundreds of thousands of entries](https://wordpress.org/support/topic/scales-to-at-least-250000-entries).
* **[CSV Export](https://connections-pro.com/documentation/tools/#Export)** of addresses, phone numbers, email addresses and dates.
* **[CSV Import](https://connections-pro.com/documentation/tools/#Categories-2)** of **nested** categories.
* **[Role capability support](https://connections-pro.com/documentation/roles/)**.
* [Displaying your business directory on the frontend is as simple as putting a shortcode on a page.](https://connections-pro.com/documentation/install/#Step_4_Displaying_your_Directory) Don't let this simplicity fool you. The shortcode provides a large array of options that are just too numerous to list. To learn more, [go here](https://connections-pro.com/documentation/plugin/shortcodes/).
* **SEO.** Every entry in your business directory outputs following the [hCard](http://microformats.org/wiki/hcard) spec. Soon to be updated to following Schema.org.
* **[Extensible](https://connections-pro.com/extensions/)** and developer friendly.
* Robust **templating support** with the [Template Customizer](https://connections-pro.com/2015/07/27/feature-preview-template-customizer/). Several basic templates are provided to get you started with you business directory with many more [premium templates available](https://connections-pro.com/templates/) to really make your business directory shine.

= Extensible =

Here are some great **free extensions** (with more on the way) that enhance your experience with Connections Business Directory:

**Utility**

* [Toolbar](https://wordpress.org/plugins/connections-toolbar/) :: Provides quick links to the admin pages from the admin bar.
* [Login](https://wordpress.org/plugins/connections-business-directory-login/) :: Provides a simple to use login shortcode and widget.
* [Anniversary and Birthday Emails](https://wordpress.org/plugins/connections-business-directory-anniversary-and-birthday-emails) :: Add the ability to automatically send a customizable email to entries on their anniversary or birthday.

**Custom Fields**

* [Business Open Hours](https://wordpress.org/plugins/connections-business-directory-hours/) :: Add the business open hours.
* [Certifications](https://wordpress.org/plugins/connections-business-directory-certifications/) :: Create and assign certifications to individuals in your directory.
* [Local Time](https://wordpress.org/plugins/connections-business-directory-local-time/) :: Add the business local time.
* [Facilities](https://wordpress.org/plugins/connections-business-directory-facilities/) :: Add the business facilities.
* [Income Level](https://wordpress.org/plugins/connections-business-directory-income-levels/) :: Add an income level.
* [Education Level](https://wordpress.org/plugins/connections-business-directory-education-levels/) :: Add an education level.
* [Languages](https://wordpress.org/plugins/connections-business-directory-languages/) :: Add languages spoken.
* [Hobbies](https://wordpress.org/plugins/connections-business-directory-hobbies/) :: Add hobbies.

**Misc**

* [Face Detect](https://wordpress.org/plugins/connections-business-directory-face-detect/) :: Applies face detection before cropping an image.

**[Premium Extensions](https://connections-pro.com/extensions/)**

* [Authored](https://connections-pro.com/add-on/authored/) :: Displays a list of blog posts written by the entry on their profile page.
* [Contact](https://connections-pro.com/add-on/contact/) :: Displays a contact form on the entry's profile page to allow your visitors to contact the entry without revealing their email address.
* [CSV Import](https://connections-pro.com/add-on/csv-import/) :: Bulk import your data in to your directory.
* [Custom Category Order](https://connections-pro.com/add-on/custom-category-order/) :: Order your categories exactly as you need them.
* [Custom Entry Order](https://connections-pro.com/add-on/custom-entry-order/) :: Allows you to easily define the order that your business directory entries should be displayed.
* [Enhanced Categories](https://connections-pro.com/add-on/enhanced-categories/) :: Adds many features to the categories.
* [Form](https://connections-pro.com/add-on/form/) :: Allow site visitor to submit entries to your directory. Also provides frontend editing support.
* [Link](https://connections-pro.com/add-on/link/) :: Links a WordPress user to an entry so that user can maintain their entry with or without moderation.
* [ROT13 Encryption](https://connections-pro.com/add-on/rot13-email-encryption/) :: Protect email addresses from being harvested from your business directory by spam bots.
* [SiteShot](https://connections-pro.com/add-on/siteshot/) :: Show a screen capture of the entry's website.
* [Widget Pack](https://connections-pro.com/add-on/widget-pack/) :: A set of feature rich, versatile and highly configurable widgets that can be used to enhance your directory.

**[Premium Templates](https://connections-pro.com/templates/)**

Connections Business Directory comes with a couple templates to get you started which fully support the [Template Customizer](https://connections-pro.com/2015/07/27/feature-preview-template-customizer/). In addition to these free templates there are many premium templates available to take your business directory the level visually.

* [Circled](https://connections-pro.com/add-on/circled/) :: A simple but bold template specifically designed for displaying small team or staff directory.
* [cMap](https://connections-pro.com/add-on/cmap/) :: Our most popular go to template for a business directory and chamber of commerce business directory, featuring full Template Customizer support.
* [Excerpt Plus](https://connections-pro.com/add-on/excerpt-plus/) :: Simple design which features a directory entry name, photo thumbnail and excerpt.
* [Gridder](https://connections-pro.com/add-on/gridder/) :: Another simple but bold template which displays the directory in a grid layout. Perfect for displaying a small team or staff directory
* [Slim Plus](https://connections-pro.com/add-on/slim-plus/) :: Specifically designed to take up as little space on the page as possible.
* [Tile Plus](https://connections-pro.com/add-on/tile-plus/) :: This template was purposely designed to display the directory in a grid or column layout.

= Developer Friendly =

* Open development on [Github](https://github.com/Connections-Business-Directory/Connections).
* Access to **180 action hooks** and over **300 filters** ... and counting.
* Term API.
* Custom Metabox and Fields API.
* Template Engine and API.
* Fragment Cache API.
* Log API (stateless and stateful).
* Admin Notices API.
* Email API.
* Settings API.

Connections Business Directory has a very flexible template engine. The loading of template and CSS are context aware. This means you could create specific templates that load when a user visits a specific entry, category, postal code and more.

Templates and CSS overriding is very granular and update safe. Check out these links for the details:

* [Custom Core CSS](https://connections-pro.com/2014/05/04/quicktip-custom-css/)
* [Custom CSS for Templates](https://connections-pro.com/2014/05/07/quicktip-custom-css-templates/)
* [Custom Template Override File](https://connections-pro.com/2014/06/04/quicktip-custom-template-override-files/)

= Languages =

Connections Business Directory has been embraced around the world and has been translated by its users in the following languages.

* Arabic [60% Complete]
* Catalan [52% Complete]
* Croatian (Croatia) [22% Complete]
* Danish [31% Complete]
* Danish (Denmark) [29% Complete]
* Dutch (Netherlands) [49% Complete]
* Finnish [78% Complete]
* French (France) [90% Complete]
* German (Germany) [86% Complete]
* Greek (Greece) [69% Complete]
* Hebrew (Israel) [61% Complete]
* Hungarian (Hungry) [52% Complete]
* Italian (Italy) [58% Complete]
* Norwegian [13% Complete]
* Persian (Iran) [52% Complete]
* Polish (Poland) [52% Complete]
* Portuguese (Brazil) [77% Complete]
* Portuguese (Portugal) [24% Complete]
* Romanian (Romania) [69% Complete]
* Russian (Russia) [40% Complete]
* Serbian (Latin) [2% Complete]
* Spanish (Latin America) [49% Complete]
* Spanish (Mexico) [99% Complete]
* Spanish (Spain) [99% Complete]
* Sweden (Swedish) [96% Complete]
* Turkish (Turkey) [58% Complete]

== Credits: ==
* Connection Business Directory was based off LBB, ["Little Black Book"](https://wordpress.org/extend/plugins/lbb-little-black-book/); which was based on [Addressbook](https://wordpress.org/extend/plugins/addressbook/), both of which can be found in the Plugin Directory.
* vCard class is a modified version by [Troy Wolf](http://www.troywolf.com/articles/php/class_vcard/).
* Update Notice in plugin admin inspired by Changelogger 1.2.8 by [Oliver Schlöbe](https://wordpress.org/extend/plugins/changelogger/).
* Screen Options class by [Janis Elsts](http://w-shadow.com/blog/2010/06/29/adding-stuff-to-wordpress-screen-options/).
* $.goMap() jQuery Google Maps Plugin by [Jevgenijs Shtrauss](http://www.pittss.lv/jquery/gomap/).
* MarkerClustererPlus jQuery Google Maps Marker Clustering Plugin by [Gary Little](http://gmaps-utility-library-dev.googlecode.com/svn/tags/markerclusterer/).
* Validation jQuery plugin by [Jörn Zaefferer](http://bassistance.de/jquery-plugins/jquery-plugin-validation/).
* Chosen jQuery plugin by [Harvest](https://github.com/harvesthq/chosen/).
* qTip jQuery plugin by [Craig Thompson](http://craigsworks.com/projects/qtip2/).
* Email and URL validation methods by [Gizmo Digital Fusion](http://wpcodesnippets.info/blog/two-useful-php-validation-functions.html).
* Social media icons by [WPZOOM.com](http://www.wpzoom.com/wpzoom/500-free-icons-wpzoom-social-networking-icon-set/); license [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/).
* iTunes icon by [Paul Robert Lloyd](http://paulrobertlloyd.com/); license [Attribution-ShareAlike 2.0 UK: England & Wales (CC BY-SA 2.0)](http://creativecommons.org/licenses/by-sa/2.0/uk/).
* Podcast icon by [schollidesign](http://findicons.com/icon/94188/podcast_new); license GNU/GPL.
* CSS theme and image used for the jQuery UI Datepicker by [helenhousandi](https://github.com/helenhousandi/wp-admin-jquery-ui).

== Screenshots ==

1. The many possible faces of your business directory. Connections Business Directory supports templates. This screenshot showcases (left to right) [Excerpt Plus](https://connections-pro.com/add-on/excerpt-plus/), [Circled](https://connections-pro.com/add-on/circled/), [Gridder](https://connections-pro.com/add-on/gridder/), [Tile Plus](https://connections-pro.com/add-on/tile-plus/) and [Slim Plus](https://connections-pro.com/add-on/slim-plus/).
2. Another screenshot showcasing the different templates that can be used to style your business directory. Left to right; [Circled](https://connections-pro.com/add-on/circled/), [cMap](https://connections-pro.com/add-on/cmap/) and [Gridder](https://connections-pro.com/add-on/gridder/).
3. The [Dashboard admin page](https://connections-pro.com/documentation/dashboard/). A snapshot of important information about the business directory.
4. The [Manage admin page](https://connections-pro.com/documentation/manage/). Here you can manage all the entries within the address book.
5. The [Add New Entry admin page](https://connections-pro.com/documentation/add-entry/). From here you can add new entries to the addressbook.
6. The [Categories admin page](https://connections-pro.com/documentation/categories/). Add as many categories as you wish to the directory. Categories do support parent/child relationships.
7. The [Templates admin page](https://connections-pro.com/documentation/templates/). Here you choose which template that the business directory should use when displaying the directory.
8. The [Settings admin page](https://connections-pro.com/documentation/settings/) where you can configure the business directory options.
9. The [Role and Capabilities admin page](https://connections-pro.com/documentation/roles/). Here you can assign which roles have which capabilities in viewing and managing the directory.
10. The [Tools :: Export admin page](https://connections-pro.com/documentation/tools/#Export) contains many different CSV export tools to allow you to easily export the data from your business directory.
11. The [Tools :: Import admin page](https://connections-pro.com/documentation/tools/#Categories-2) allows you to quickly bulk import nested categories into your business directory.
12. In the event that you need support the [Tools :: System Information admin page](https://connections-pro.com/documentation/tools/#System_Information) contains all the information needs about your directory installation which can be easily and quickly shared.
13. Need to move the configuration of your business directory from one site to another? The [Tools : Settings Import/Export admin page](https://connections-pro.com/documentation/tools/#Settings_ImportExport) allows you to do it with zero fuss. This will even migrate the Template Customizer settings applied to template and any of the settings for the [premium extensions](https://connections-pro.com/extensions/).

[Connections running on live websites can be found here.](https://connections-pro.com/showcase/)

== Frequently Asked Questions ==

A comprehensive list of [FAQs can be found here.](https://connections-pro.com/faq/)

= How do I install Connections? ==
[Installation instructions can be found here.](https://connections-pro.com/documentation/plugin/install/)

Using the WordPress Plugin Search

1. Navigate to the `Add New` sub-page under the Plugins admin page.
2. Search for `connections business directory`.
3. The plugin should be listed first in the search results.
4. Click the `Install Now` link.
5. Lastly click the `Activate Plugin` link to activate the plugin.

Uploading in WordPress Admin

1. [Download the plugin zip file](https://wordpress.org/plugins/connections/) and save it to your computer.
2. Navigate to the `Add New` sub-page under the Plugins admin page.
3. Click the `Upload` link.
4. Select Connections Business Directory zip file from where you saved the zip file on your computer.
5. Click the `Install Now` button.
6. Lastly click the `Activate Plugin` link to activate the plugin.

Using FTP

1. [Download the plugin zip file](https://wordpress.org/plugins/connections/) and save it to your computer.
2. Extract the Connections Business Directory zip file.
3. Create a new directory named `connections` directory in the `../wp-content/plugins/` directory.
4. Upload the files from the folder extracted in Step 2.
4. Activate the plugin on the Plugins admin page.

= How do I display the business directory on my site? =
We have a [QuickStart](https://connections-pro.com/quickstart/) available that'll walk you thru the most basic setup. Basically all you need to do is, create a page, and add the `[connections]` shortcode and then start adding entries to your directory.

= Will it work with my theme? =
Connections Business Directory has been designed to work with any theme.

= Is Connections Business Directory translation-ready? =
Yes it is. Connections Business Directory comes with many user supplied translations. We use Transifex to manage translations. This service make it easy for us manage and easy for you to translate. To read more, see [this page](https://connections-pro.com/documentation/translation/).

= Is Connections Business Directory compatible with WordPress Multisite? =
Yes it is. However, do not Network activate Connections. Activate it on only the subsites that you wish to use Connections.

= Is it possible to share entries with other sites within a WordPress Multisite installation? =
Yes this is possible but there is a special setup required to do so. It is recommended this is done before added entries to your business directory.

1. Activate Connections Business Directory the primary site.
2. Add `define( 'CN_MULTISITE_ENABLED', FALSE );` to your `wp-config.php` file. **NOTE:** If you have added any entries to any of your subsites, that data will be lost as Connections will read the directory entry data from the primary site's database tables.
3. Activate Connections Business Directory on the subsites you wish to use Connections.

= What are the requirements to run Connections? =
* **WordPress version:** >= 4.4
* **PHP version:** >= 5.2.4 ( >= 7.0 is highly recommended)
* **NOTE:** Upgrading from version 0.6.1 and newer only, is supported. Previous version must upgrade to 0.6.1 before upgrading to the current version.

== Changelog ==

[Complete Changelog can be found here.](https://connections-pro.com/changelog/)

= 8.43 05/17/2019 =
* TWEAK: Do not save previous entry query results in an array. Doing this made doing REST changes have odd results.
* BUG: Check for `WP_Error` before calling `cnCountry::getDivisions()` to prevent a potential fatal PHP error.
* BUG: Ensure the permalink option for an entry image is converted to a bool.
* BUG: When loading Gmagick, properly set the default image quality.
* BUG: Additional checks to prevent PHP notices when using the default geocoder.
* OTHER: Readme tags tweaks to stay within proposed plugin guidelines.
* DEV: Remove unused code.

= 8.42.1 05/13/2019 =
* NEW: Introduce the `cn_export_add_bom` filter.
* BUG: Correct regression from last update which could cause icon font conflicts.
* BUG: Correct the brandicon version when enqueuing the CSS.

= 8.42 05/10/2019 =
* NEW: Introduce `cnRetrieve::getEntryBy()`.
* COMPATIBILITY: Rename the icomoon font-family to Connections-Brandicons to prevent naming conflicts with theme's and other plugins which have not renamed their icomoon icon font libraries.
* COMPATIBILITY: Dequeue the Striking theme CSS on the Connections admin pages.
* TWEAK: Readme.txt tweaks.
* TWEAK: Check if object property is an array before counting to prevent PHP warnings.
* TWEAK: Tweak admin CSS enqueuing to enqueue on the required CSS on specific Connections admin pages.
* TWEAK: Admin CSS tweaks to ensure Brandicons and FontAwesome do not conflict on the Connections admin pages.
* OTHER: Remove commented out HTML code from the settings admin page.
* OTHER: Bump Tested up to version 5.2.
* OTHER: Continue laying the foundation for the font icon picker support for the social network settings.
* OTHER: Update minified admin CSS files.
* BUG: Correct social networks fieldset settings ID.
* DEV: phpDoc correction.
* DEV: Update dist build files.

= 8.41.2 05/02/2019 =
* TWEAK: Do not clear the addon update check cache when doing addon status check. This causes unnecessary addon update checks to occur.
* TWEAK: Since supplied last_checked property is being restored before returning the transient data in update checks, no need to set it with time().

= 8.41.1 05/02/2019 =
* TWEAK: Use the cached last_checked value from the `cn_update_plugins` option instead of the value supplied by WordPress since third party plugin libraries alter it in a way that can cause excessive plugin update checks.
* TWEAK: Restore timeout value for license status checks.
* TWEAK: Remove the action to clear the `cn_update_plugins` option when `wp_clean_plugins_cache()` is called to reduce frequency of addon update checks.

= 8.41 04/30/2019 =
* NEW: Add option to display country names in the address country autocomplete in either English or in their native translation. The later being the default which matches core WordPress when displaying country names for the site language preference.
* TWEAK: Tweak to expiration of cached extension status checks.
* OTHER: Update EDD-SL Plugin Updater library from 1.6.14 to 1.6.18.
* TWEAK: Do not subtract the timeout value from time when setting up the last checked property.

= 8.40.2 04/26/2019 =
* TWEAK: Update the user agent to match the extension plugin info request.

= 8.40.1 04/25/2019 =
* TWEAK: Tweak to expiration of cached extension update checks.
* DEV: Reverse change to composer.json.

= 8.40 04/19/2019 =
* NEW: Add brand font icons and use webpack to process the frontend CSS.
* NEW: Update the social media icons to use the brandicons.
* TWEAK: Default image quality should be `82` not `80` to match WP core.
* TWEAK: Remove the legacy/advanced single entry template option.
* TWEAK: Suppress the display of the "Custom Fields" metabox by default on new installations matching core WordPress which hide its "Custom Fields" metabox by default unless enabled by the current user.
* OTHER: Remove the WP REST API shims since they are no longer required as Connections require WP >= 4.7.12 (WP REST API was added in WP 4.7).
* OTHER: Bump minimum required PHP version to 5.6.20 to match WP 5.2.
* OTHER: Include the Font Awesome index source maps.
* OTHER: Start laying the foundation for the font icon picker support for the social network settings.
* BUG: Ensure the permalink setting for the entry image respects the permalinks setting.
* DEV: Update `clean-webpack-plugin`.
* DEV: Manage Chosen and Picturefill 3rd party vendor assets using npm/webpack.
* DEV: Update Chosen and Picturefill 3rd party vendor asset paths when registering with WP.
* DEV: Manage Fontawesome and fontIconPicker 3rd party vendor assets using npm/webpack.
* DEV: Remove npm as project level dependency.
* DEV: Update the npm autoprefixer dependency.
* DEV: Update the package.json engine for npm.
* DEV: Commit the updated package-lock.json file.
* DEV: Use @wordpress/default as the babel-loader-default.
* DEV: Match core WordPress autoprefixer config.

= 8.39.1 04/09/2019 =
* TWEAK: Update links to plugin site from `http` to `https`.
* TWEAK: Remove link to features requests forum from dashboard quick links.
* TWEAK: Update link to support in plugin meta actions links.
* TWEAK: Add support link to admin nav to make it more discoverable for the user.
* TWEAK: Rename the Messenger IM option to Live Messenger so it is not confused with the Facebook Messenger.
* TWEAK: Do not make Live Messenger IM user ID links since the service was cancelled my MS quite awhile ago.
* TWEAK: Adjust processing of Advanced Block Options for the Team block so the internal limit value can be overridden.
* BUG: Remove debug code from the Directory block.
* BUG: Correct the array index for the excluded categories in the Team block.
* OTHER: Bump minimum required PHP version to 5.6 to match WordPress core.

= 8.39 04/03/2019 =
* FEATURE: Introduce the new Team block.
* TWEAK: Sort countries from their native name in the country drop down.
* TWEAK: Core WP changes some default admin styles. Tweak the class names assigned to the Manage admin page character index so the render correct in < WP 5.1.
* BUG: Honor the public/private override settings when determining if the current user can view.
* BUG: Do not import the SCSS files in the Directory block since they are unused.
* BUG: Fix undefined index PHP notice when data type was disabled but entry had that type assigned. Now it defaults to the default data type.
* DEV: Organize the block components into sub folders.
* DEV: Alias `@Connections-Directory` in webpack so components could be imported using that alias.
* DEV: Add JSDoc to the blocks.
* DEV: phpDoc correction.

= 8.38.1 02/20/2019 =
* TWEAK: Logic tweak to prevent array_map() PHP notices from being generated by the shortcode.
* TWEAK: No need to assign `$response` on return.
* TWEAK: Check for `preg_split()` fail.
* OTHER: Add missing changelog header.
* DEV: phpDoc Correction.

= 8.38 02/19/2019 =

* FEATURE: Introduce Filters to the Editor Directory block as autocomplete fields to bring the block into feature parity with the shortcode.
* NEW: Introduce the AutoComplete REST API endpoint.
* TWEAK: Refactor the Upcoming List block to return a `Fragment` vs. an array to stop React browser console error messages about missing key.
* TWEAK: Load block editor scripts in the footer.
* TWEAK: Use `cnSanitize::args()` instead of `wp_parse_args()` in `cnTemplatePart::listAction_ViewAll()` to strip unnecessary args to prevent PHP error messages when displaying the character index.
* TWEAK: Allow shortcode callback to receive arrays for the filters to support the Directory Editor block.
* OTHER: Update Author URL in plugin header.
* OTHER: Update URL property in package.json.
* OTHER: Remove console logging of the category store.
* OTHER: Update frontend minified CSS file.
* DEV: Make webpack a little less chatty.
* DEV: Add `$` as an external dependency to webpack.
* DEV: phpDoc correction.
* DEV: Add `lodash` as a block editor dependency.
* DEV: Update build files.

= 8.37 02/04/2019 =
* FEATURE: Add searchable category checklists to the Directory block to allow excluding/including the selected categories in the directory.
* NEW: Register the category store.
* NEW: Introduce the `HierarchicalTermSelector` editor component for rendering a searchable category checkbox group.
* TWEAK: Bump minimum support WP version to 4.7.12.
* TWEAK: CSS tweak to the search input to help keep themes from breaking it.
* TWEAK: Do not use `time()` for version when enqueueing the editor script. Used current plugin version + last modified time.
* BUG: It seems the post type meta needs to be passed back as a return value from `withSelect()` in the page select control so the pages are properly indented for their hierarchy.
* BUG: Correct category taxonomy REST base endpoint.
* OTHER: Correct mistaken default and named imports/imports.
* OTHER: Add whitespace.
* OTHER: Remove unused code from directory block.
* OTHER: Correct code alignment.
* DEV: Reorder package.json keys.
* DEV: Add JSDoc comment blocks.
* DEV: Tweak Travis config.
* DEV: Update Webpack to latest.
* DEV: Update build files.

= 8.36.1 01/15/2019 =
* TWEAK: Update block editor javascript dependencies to include all WP core editor dependencies.
* TWEAK: Import `withInstanceId` from `@wordpress/compose` instead of pulling it from the global `wp` object.
* TWEAK: Ensure `$orderByAtts` is defined to prevent PHP warning.
* TWEAK: Add a few more styles to help prevent themes from breaking the map.
* TWEAK: Add a few prefixed CSS attributes for increased browser compatibility.
* OTHER: Update copyright year.
* DEV: Update dev dependencies.
* DEV: Update minified files.
* DEV: Update build files.

= 8.36 01/14/2019 =
* NEW: Add additional options to the Directory block to bring it closer to feature parity with the `[connections]` shortcode.
* NEW: Allow ordering by any date type, not just anniversary and birthday.
* NEW: Introduce `cnOptions::getEntryTypeOptions()`.
* NEW: Introduce `cnTemplateFactory::getOptions()`.
* NEW: Add an Editor PageSelect component.
* TWEAK: Minor CS tweak for mobile device display for the Profile template.
* TWEAK: Add additional check before initiating a template class to prevent PHP errors.
* TWEAK: No need to add slashes to CSV export cell data.
* TWEAK: Add BOM to CSV export file because Excel is dumb.
* TWEAK: Add additional option values which the blocks can use as defaults.
* BUG: When displaying OSM maps tiles, do not display the layers control.
* OTHER: Update some country meta.
* DEV: Code formatting.
* DEV: Move the range-control component to shared folder for all editor blocks.

= 8.35 12/27/2018 =
* NEW: Add filters to allow addons to hook into the entry manage action links.
* TWEAK: Change text domain registration to priority 1 to support WP Globus.
* TWEAK: Normalize date input to YYYY-MM-DD.
* TWEAK: Remove unused in page section links on the Connections : manage admin page.
* I18N: Update POT file.
* I18N: Update MO files.
* DEV: Tweak Travis CI config.

= 8.34 12/14/2018 =
* TWEAK: Remove use of `create_function()` throughout.
* TWEAK: Bump max tested PHP to 7.3.
* BUG: Max WP version should be `5.0`.
* BUG: Do not use PHP 5.3 syntax in cnText_Domain to prevent fatal errors in PHP < 5.3.
* BUG: Fallback to `get_locale()` from `get_user_locale()` to prevent fatal errors on WP < 4.7.
* BUG: Removed unused variable in `cnTerm::gettaxonomyTerms()`.
* DEV: phpDoc corrections.

= 8.33 12/06/2018 =
* NEW: Add support for displaying the original year and years since in the Upcoming List Gutenberg block
* NEW: Add support for displaying the original year and years since in the `[upcoming_list]` shortcode.
* TWEAK: Add help text to the Directory block options.

= 8.32 11/30/2018 =
* BREAKING CHANGE: Renamed the block namespace. If you used the Connections block in WP 5.0-rc1, you'll need to update the block.
* FEATURE: Introduce the Upcoming List block for the WordPress 5.0 editor.
* NEW: Introduce a `no_results` option to the `[upcoming_list]` shortcode so the no results message can be customized.
* NEW: Introduce `cnDate::GetUpcoming()`.
* TWEAK: No need to use the `editor_script` parameter in `register_block_type()` since is is enqueued in the `enqueue_block_editor_assets` hook.
* TWEAK: Check for the `wp_set_script_translations()` function before registering blocks. It seems this function does not exist when using the Gutenberg plugin. So, lets not support the Gutenberg plugin and support only WP 5.0.
* TWEAK: Update the anniversary and birthday template with more specific CSS so they do no conflict.
* TWEAK: Rename the Directory block advanced settings field.
* TWEAK: Add missing period to upcoming list heading.
* TWEAK: Add a default heading to the upcoming list.
* TWEAK: Remove unused code from upcoming list.
* TWEAK: The `list_title` option for the `[upcoming_list]` shortcode should default to an empty string and not `null`.
* TWEAK: Update the dark and light templates for Anniversaries and Birthdays so they support all registered date types.
* BUG: Do not attach Chosen to the country field when the field is disabled.
* DEV: Add some inline comments to describe the `register_block_type()` parameters since they are not outlined in the Gutenberg documentation.
* DEV: Add inline comment for the `html` parameter for the `registerBlockType()` function.
* DEV: Reorder the deconstructed `wp.i18n` methods.
* DEV: Add classnames and lodash as dev dependencies to package.json.

= 8.31 11/23/2018 =
* NEW: WordPress 5.0 Support.
* FEATURE: Gutenberg Block to which can be used to insert the directory into the page.
* TWEAK: Update the attribution link to Leaflet.
* TWEAK: Exclude the Leaflet CSS from Autoptimize.
* TWEAK: The custom HTML elements for the map layers control should be hidden.
* TWEAK: Remove unnecessary `is_admin()` check when registering CSS.
* DEV: Update package.json with dev dependencies to support webpack.
* DEV: Initial babel and webpack config.

= 8.30.1 11/09/2018 =
* BUG: Update map blocks minified JavaScript assets.

= 8.30 11/08/2018 =
* NEW: Introduce `cnOptions::getBaseGeoCoordinates()`.
* TWEAK: Use current version for the Map Block JS script for cache busting.
* TWEAK: Rename file which registers the settings options.
* TWEAK: Default the `[cn-mapblock/]` shortcode map center to the user defined country/region.
* BUG: Properly detect if section is a WP core settings section so it is not prefixed with the plugin ID.
* OTHER: Begin work on modern settings API.
* DEV: phpDoc correction.
* DEV: Fix code alignment/flow.

== Upgrade Notice ==

= 8.30 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.30.1 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.31 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.32 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.33 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.34 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.35 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.36 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.36.1 =
It is recommended to backup before updating. Requires WordPress >= 4.5.3 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.37 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.38 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.38.1 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.39 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.4 PHP version >= 7.1 recommended.

= 8.39.1 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.40 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.40.1 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.40.2 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.41 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.41.1 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.41.2 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.42 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.42.1 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.

= 8.43 =
It is recommended to backup before updating. Requires WordPress >= 4.7.12 and PHP >= 5.6.20 PHP version >= 7.1 recommended.
