/**
 * Register the data stores.
 */
import './store/categories.js';

/**
 * Import the blocks
 */
import './directory/index.js';
import './team/'
import './upcoming/index.js';
