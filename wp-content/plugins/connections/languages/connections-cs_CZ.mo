��    m      �  �   �      @	  9   A	  6   {	  >   �	     �	     �	     
     
     
      
     &
     .
     7
     C
  (   G
     p
     }
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
  !   �
               )  %   .  
   T     _     e     m     t     �     �     �     �     �     �  
   �     �     �               !  
   (     3     9     >     F     N     S     V     ]     a     f     o     u     z          �     �     �     �     �     �     �  	   �     �  #   �     �     �     �     �                    $     +     8     D  
   P     [     `     i     n     v     }     �     �     �     �     �     �     �     �     �     �  
   �  4   �                    4     P  �  _            =        S     [     p     �     �     �  	   �     �  	   �     �     �     �     �     �       
                   1     ?     G     S     Z     `     f     w     �     �     �     �  	   �     �     �  
   �     �     �     �     �     �  
               	         *  
   0     ;     I     P     S  	   [     e     k  
   n     y  	   �     �     �     �  	   �     �  	   �     �     �     �     �     �     �     �  
   �  *   	  	   4     >     K  	   R     \     a     m          �  
   �     �     �  	   �  
   �  	   �  	   �     �     �     �     �     �                     0     8  	   @     J     V  7   b     �     �  	   �     �     �     Z   b   &                 ,      h                   ^                  S      W   %   "   6   G   !                V   1      _   
   +   B          $       0   8       .   9   j         k   O   #   -   7   i   a   /   @   5   H   C   f   K              =       d   Y       \       A      :   D       E   P   X          U           F         c       m       4      L       J   )   >   	   2   R   l           [              *       ]       ?   T                   '             Q   3   N   g                  ;      (   M      <               `       I       e    A link (or hyperlink, or web link) title attribute.Title A name that describes someone's position or job.Title A term with the name provided already exists in this taxonomy. Activate Add New Add New Category Advanced All Apply Approve Approved Attachments BCC Blind courtesy copy email addresses.BCC Bulk Actions CC Cancel Categories Categories: Category Clear Compatibility Content of email.Message Continue Count Country Courtesy copy email addresses.CC Current Color Current page Date Date and time an email was sent.Date Deactivate Debug Default Delete Description Display Do Not Import Documentation Edit Edit Category Email Address Email Type Email recipients (To).To Email sender (From).From Email subject.Subject Error Export Extensions First From General Headers Home ID Import Key Last Licenses Links Logo Logs Message Moderate Name None Number Other Page Parent Permalink Photo Please enter a valid email address. Private Publish Region Remove Roles Search Search Categories Select Select Color Select Logo Select Page Send Email Sent Settings Slug Subject Submit Title To Type URL Update Update Category Upgrade Upload Value View View %s Visibility You do not have permission to install plugin updates of postAdd New post type general nameLogs taxonomy singular nameType term nameName Project-Id-Version: Connections
Report-Msgid-Bugs-To: http://connections-pro.com/support/forum/translations/
POT-Creation-Date: 2018-12-26 17:57:11+00:00
PO-Revision-Date: 2018-12-26 17:57+0000
Last-Translator: Steven Zahm <shazahm1@gmail.com>
Language-Team: Czech (Czech Republic) (http://www.transifex.com/shazahm1/connections/language/cs_CZ/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs_CZ
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
X-Generator: grunt-wp-i18n1.0.1
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: UTF-8
X-Textdomain-Support: yes
 Titulek Titulek Položka se zvoleným názvem už v této taxonomii existuje. Zapnout Vytvořit formulář Přidat novou kategorii Další nastavení Vše Použít Schválit Schválené Přílohy Skrytá kopie Skrytá kopie Hromadné úpravy Kopie Zrušit Rubriky Kategorie: Rubrika Vymazat pořadí Kompatibilita Zpráva Pokračovat Počet Země Kopie Aktuální barva Aktuální stránka datum datum Vypnout Ladění Výchozí Smazat Popis Zobrazení Neimportovat Dokumentace Upravit Upravit rubriky E-mailová adresa Typ emailu Komu Od Předmět Chyba Exportovat Rozšíření První Od Obecný Záhlaví Domů ID Importovat Klíč Poslední Licence Odkazy Logo Protokoly Zpráva Moderovat Jméno Žádné Číslo Ostatní Stránka Nadřazené Trvalý odkaz Fotografie Zadejte prosím platnou e-mailovou adresu. Soukromé Publikováno Region Odstranit Role Vyhledání Prohledat rubriky Vybrat Zvolit barvu Vyber logo Vyberte stránku Poslat e-mail Odeslané Nastavení Permalink Předmět Odeslat Titulek Komu Typ URL Aktualizovat Uložit rubriky v "aktualizaci" Nahrát Hodnota Zobrazit: Zobrazit %s Viditelnost Nemáte oprávnění pro instalaci aktualizací pluginu / Vytvořit formulář Protokoly Typ Jméno 