��    �      ,  �   <      �
  9   �
  6   +     b     n     w     �     �     �     �     �     �     �  	   �     �               8     =     Q     c     l     q     y  
   �     �     �      �  
   �     �                    !     3     ;     B  %   G     m     v     �     �     �     �     �     �  
   �     �     �     �     �     �               !     4     9  
   B     M     U     X     ^     e     k  
   p     {     �     �     �  
   �     �     �     �     �     �     �     �     �     �     �                  
   .  	   9     C     K     R     Z     f     m     {     �     �     �     �     �     �     �     �     �     �     �  
   �                    !     /     ;     G     S     \     a     h     �     �     �     �     �  
   �     �     �     �  
   �     �  
   �     �     �     �  B       H     O     V     g     u     �     �     �     �     �     �     �          
          ,     D     J     ]     f     n     t  
   y  
   �     �     �     �     �     �     �     �     �  	                            #     *     7     =     A     O     T     b     q     �     �  	   �     �     �     �     �     �     �     �     �     �     �     �     �            
          	        &     +  
   1     <     D     J     N     c     �     �     �     �     �     �     �     �     �     �     �  	         
                &     6     D     W     ]  
   b  	   m     w     �     �     �     �     �     �  	   �     �     �  	   �     �     �  
   �                         8     ?     M     S     Z  	   c     m     v     {     �  	   �     �     �  
   �     �           r       S   z   h   !   {   2         s   -           3      ^      y   	   T   m   9           ;   L       F   t   O   >       8   ,   ]       H       G   Q   |           1       =   b   C      a      %   p   Z   N   k       4       (   v          d              *   K       j       "   P       U      J   .       6      
         V       }   u       `           /          '   �   g   \   q   <       5      W             i       $   D       n      B       �      Y   [   ?          e   )   o   E       +          X   f   &         @      #      c   x   I   �             ~               7   w       R          A       0   :               M   _   l    A link (or hyperlink, or web link) title attribute.Title A name that describes someone's position or job.Title Add Address Add Date Add Email Address Add Link Add Phone Number Add to Address Book. Address Line 1 Address Line 2 Address Line 3 Address Type Addresses Advanced Assign link to the image. Assign link to the logo. Aunt Awaiting Moderation Biographical Info Birthday Blog Brother Brother-in-law Categories Category has been added. Category has been updated. Category(ies) have been deleted. Cell Phone Certification City Contact Contact First Name Contact Last Name Country Cousin Date Date and time an email was sent.Date Daughter Daughter-in-law Delete Display Documentation ERROR Email Address Email Addresses Email Type FALSE Father Father-in-law Friend Grand Father Grand Mother Great Grand Father Great Grand Mother Home Home Fax Home Phone Husband ID Image Images Links Logo Membership Mother Mother-in-law Name Nephew New Window News Niece No No Birthdays Today No updates at this time Notes On Organization Other Partner Personal Email Phone Number Phone Numbers Phone Type Preferred Private Public Publish Quick Links Remove Require Login Results List Same Window School Search Select Image Select Logo Select Relation Settings Sister Sister-in-law Social Network Son Son-in-law Spouse State Step Brother Step Daughter Step Father Step Mother Step Sister Step Son TRUE Target The entry has been approved. Title Uncategorized Uncle Unknown Update Visibility Website Wife Work Work Email Work Fax Work Phone Yes Zipcode term nameName Project-Id-Version: Connections
Report-Msgid-Bugs-To: http://connections-pro.com/support/forum/translations/
POT-Creation-Date: 2018-12-26 17:57:11+00:00
PO-Revision-Date: 2018-12-26 17:57+0000
Last-Translator: Steven Zahm <shazahm1@gmail.com>
Language-Team: Norwegian (http://www.transifex.com/shazahm1/connections/language/no/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: no
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: grunt-wp-i18n1.0.1
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: UTF-8
X-Textdomain-Support: yes
 Tittel Tittel Legg til adresse Legg til dato Legg til e-postadresse Legg til lenke Legg til telefonnummer Legg til i adressebok Adresselinje 1 Adresselinje 2 Adresselinje 3 Adressetype Adresser Avansert Legg til lenke til bilde Legg til lenke til logo Tante Venter godkjenning Biografi Bursdag Blogg Bror Svigerbror Kategorier Kategori er lagt til. Kategori er oppdatert. Kategori(er) er slettet. Mobiltelefon Sertifisering By Kontakt Fornavn Etternavn Land Kusine Dato Dato Datter Svigerdatter Slett Vis Dokumentasjon FEIL E-postadresse E-postadresser E-post kategori FALSE Far Svigerfar Venn Bestefar Bestemor Tippoldefar Tippoldemor Hjemme Personlig faks Hjemmetelefon Ektemann ID Bilde Bilder Lenker Logo Medlemskap Mor Svigermor Navn Nevø Nytt vindu Nyheter Niese Nei Ingen bursdager idag Ingen oppdatering tilgjengelig Notater På Organisasjon Annet Partner Personlig e-post Telefonnummer Telefonnummer Telefonnummer kategori Foretrukket Privat Offentlig Publiser Hurtiglenker Fjern Krev innlogging Resultatliste Eksisterende vindu Skole Søk Velg bilde Velg logo Velg forhold Innstillinger Søster Svigersøster Sosiale nettverk Sønn Svigersønn Ektefelle Fylke Stebror Stedatter Stefar Stemor Stesøster Stesønn TRUE Mål Inntastingen er blitt godkjent Tittel Ukategorisert Onkel Ukjent Oppdater Synlighet Nettside Kone Arbeid Jobb e-post Jobb faks Telefon arbeid Ja Postnummer Navn 