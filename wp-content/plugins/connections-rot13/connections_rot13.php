<?php
/**
 * An extension for the Connections Business Directory plugin which adds the ability to add and assign certifications to your business directory entries.
 *
 * @package   Connections Business Directory Extension - ROT13 Email Encryption
 * @category  Extension
 * @author    Steven A. Zahm
 * @license   GPL-2.0+
 * @link      http://connections-pro.com
 * @copyright 2018 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections Business Directory Extension - ROT13 Email Encryption
 * Plugin URI:        https://connections-pro.com/add-on/rot13-email-encryption/
 * Description:       An extension for the Connections Business Directory plugin which encrypts the email addresses with ROT13 encryption in the frontend.
 * Version:           2.0
 * Author:            Steven A. Zahm
 * Author URI:        https://connections-pro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       connections_rot13
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Connections_ROT13' ) ) {

	class Connections_ROT13 {

		/**
		 * @var string
		 */
		const VERSION = '2.0';

		/**
		 * @since 2.0
		 * @var string
		 */
		const ANCHOR_PATTERN = '/(<a.*?mailto)(.*?)(<\/a>)/i';

		/**
		 * @since 2.0
		 * @var string
		 */
		const EMAIL_PATTERN = '/(<a.*?mailto:.*?>)(.*?)(<\/a>)/i';

		/**
		 * Stores the instance of this class.
		 *
		 * @since  2.0
		 *
		 * @var Connections_ROT13
		 */
		private static $instance;

		/**
		 * @var string The absolute path this this file.
		 *
		 * @since 2.0
		 */
		private $file = '';

		/**
		 * @var string The URL to the plugin's folder.
		 *
		 * @since 2.0
		 */
		private $url = '';

		/**
		 * @var string The absolute path to this plugin's folder.
		 *
		 * @since 2.0
		 */
		private $path = '';

		/**
		 * @var string The basename of the plugin.
		 *
		 * @since 2.0
		 */
		private $basename = '';

		/**
		 * Connections_ROT13 constructor.
		 */
		public function __construct() { /* Do nothing here */ }

		/**
		 * @since 2.0
		 *
		 * @return Connections_ROT13
		 */
		public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Connections_ROT13 ) ) {

				self::$instance = $self = new self;

				$self->file     = __FILE__;
				$self->url      = plugin_dir_url( $self->file );
				$self->path     = plugin_dir_path( $self->file );
				$self->basename = plugin_basename( $self->file );

				/**
				 * This should run on the `plugins_loaded` action hook. Since the extension loads on the
				 * `plugins_loaded` action hook, load immediately.
				 */
				cnText_Domain::register(
					'connections-business-directory-rot13-email-encryption',
					$self->basename,
					'load'
				);

				if ( ! is_admin() ) {

					add_filter( 'cn_output_email_address', array( $self, 'outputROT13' ) );
				}

				add_action( 'wp_enqueue_scripts', array( $self, 'loadScripts' ) );

				// License and Updater.
				if ( class_exists( 'cnLicense' ) ) {

					new cnLicense( __FILE__, 'ROT13 Email Encryption', self::VERSION, 'Steven A. Zahm' );
				}
			}

			return self::$instance;
		}

		/**
		 * @since 2.0
		 *
		 * @return string
		 */
		public function getBaseURL() {

			return $this->url;
		}

		/**
		 * @param string $string
		 *
		 * @return string
		 */
		public function applyROT13( $string ) {

			return str_replace(
				".",
				"\056",
				str_replace( "@", "\100", str_rot13( $string ) )
			);
		}

		/**
		 * @param $string
		 *
		 * @return string
		 */
		public function renderNoScript( $string ) {

			return '<noscript><span style="unicode-bidi: bidi-override; direction:rtl;">' . antispambot( strrev( $string ) ) . '</span></noscript>';
		}

		/**
		 * Callback for the `cn_output_email_address` filter.
		 *
		 * @param $out
		 *
		 * @return string
		 */
		public function outputROT13( $out ) {

			return preg_replace_callback( self::ANCHOR_PATTERN, array( &$this, 'ROT13_callback' ), $out );
		}

		/**
		 * @param array $matches
		 *
		 * @return string
		 */
		public function ROT13_callback( $matches ) {

			preg_match( self::EMAIL_PATTERN, $matches[0], $match );

			$matches[] = $match[2];

			return '<span class="qrpelcg">' . $this->applyROT13( $matches[0] ) . '</span>' . $this->renderNoScript( $matches[4] );
		}

		/**
		 * Loads the Connections javascript on the WordPress frontend.
		 */
		public function loadScripts() {

			// If SCRIPT_DEBUG is set and TRUE load the non-minified CSS files, otherwise, load the minified files.
			$min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
			$url = cnURL::makeProtocolRelative( Connections_ROT13()->getBaseURL() );

			wp_enqueue_script(
				'cn_rot13_js',
				$url . "cn_rot13{$min}.js",
				array( 'jquery' ),
				self::VERSION,
				TRUE
			);
		}

	}

	/**
	 * Start up the extension.
	 *
	 * @since  2.0
	 *
	 * @return Connections_ROT13|false
	 */
	function Connections_ROT13() {

		if ( class_exists('connectionsLoad') ) {

			return Connections_ROT13::instance();

		} else {

			add_action(
				'admin_notices',
				function() {
					echo '<div id="message" class="error"><p><strong>ERROR:</strong> Connections Business Directory must be installed and active in order use Connections Business Directory Extension - ROT13 Email Encryption.</p></div>';
				}
			);

			return FALSE;
		}
	}

	/**
	 * Since Connections loads at default priority 10, and this extension is dependent on Connections,
	 * we'll load with priority 11 so we know Connections will be loaded and ready first.
	 */
	add_action( 'plugins_loaded', 'Connections_ROT13', 11 );
}
