/**
 * Encrypts or decrypts the supplied string using ROT13 encryption
 *
 * @author Steven A. Zahm
 */
(function( $ ) {

	cnROT13 = {

		write: function( str ) {

			return str.replace( /[a-zA-Z]/g, function( character ) {

				return String.fromCharCode( ( character <= 'Z' ? 90 : 122 ) >= ( character = character.charCodeAt(0) + 13 ) ? character : character - 26 );
			});
		},
		decodeEntities( encodedString ) {

			var textArea = document.createElement( 'textarea' );
			textArea.innerHTML = encodedString;
			return textArea.value;
		}
	};

	$( 'span.qrpelcg' ).each( function() {

		var $this = $( this );
		var rot13 = $this.html();
		var email = cnROT13.write( cnROT13.decodeEntities( rot13 ) );

		$this.replaceWith( email );
	});

})( jQuery );
