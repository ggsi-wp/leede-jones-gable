jQuery(document).ready(function($){

	//Nav active states
	var pathname = window.location.pathname;

	if( pathname == '/client-dashboard/' ){
		$('.lj-client-navigation a[data-page="dash"]').addClass('nav-active');
	}else if( pathname == '/doxim-documents-page/' ){
		var parts = window.location.search.substr(1).split("&");
		var $_GET = {};
		for (var i = 0; i < parts.length; i++) {
    		var temp = parts[i].split("=");
    		$_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    	}
    	if( $_GET['tab'] == 'account' ){
    		$('.lj-client-navigation a[data-page="monthly-statements"]').addClass('nav-active');
    	}else if( $_GET['tab'] == 'trade' ){
    		$('.lj-client-navigation a[data-page="trade-confirmations"]').addClass('nav-active');
    	}else if( $_GET['tab'] == 'tax' ){
    		$('.lj-client-navigation a[data-page="tax-documents"]').addClass('nav-active');
    	}else if( $_GET['tab'] == 'reports' ){
    		$('.lj-client-navigation a[data-page="account-reports"]').addClass('nav-active');
    	}
	}else if( pathname == '/sma-quarterlies/' ){
		$('.lj-client-navigation a[data-page="sma"]').addClass('nav-active');
	}else if( pathname == '/account-forms/' ){
		$('.lj-client-navigation a[data-page="account-forms"]').addClass('nav-active');
	}else if( pathname == '/welcome-package/' ){
		$('.lj-client-navigation a[data-page="welcome-package"]').addClass('nav-active');
	}else if( pathname == '/manage-account/' ){
		$('.lj-client-navigation a[data-page="change-password"]').addClass('nav-active');
	}

});